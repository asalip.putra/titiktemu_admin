<?php
/**
 * BCA Setting & API Credentials
 */

return [
        'main'        => [
            'corp_id'       => env('BCA_CORP_ID'),
            'client_id'     => env('BCA_CLIENT_ID'),
            'client_secret' => env('BCA_CLIENT_SECRET'),
            'api_key'       => env('BCA_API_KEY'),
            'secret_key'    => env('BCA_SECRET_KEY'),
            'timezone'      => 'Asia/Jakarta',
            'host'          => 'sandbox.bca.co.id',
            'scheme'        => 'https',
            'development'   => true,
            'options'       => [],
            'port'          => 443,
            'timeout'       => 30,
        ]
];