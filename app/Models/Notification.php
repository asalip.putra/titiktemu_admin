<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Comment;
use App\Models\Feed;
use App\Models\News;
use App\Models\Product;
use App\Models\Market;
use App\Models\Order;

/**
 * Class Notification
 * @package App\Models
 * @version September 4, 2019, 10:30 am UTC
 *
 * @property \App\Models\NotificationType notificationType
 * @property \App\Models\User user
 * @property string type
 * @property string read
 */
class Notification extends Model
{

    public $table = 'notifications';
    protected $primaryKey = 'id'; // or null
    public $incrementing = false;
    public $fieldObject = null;


    public $fillable = [
        'type',
        'read_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'type' => 'string',
        'read_at' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'type' => 'required',
    ];

    /**
     * New Attributes
     *
     * @var array
     */
    protected $appends = [
        'custom_fields',
        'field_object'
    ];

    public function customFieldsValues()
    {
        return $this->morphMany('App\Models\CustomFieldValue', 'customizable');
    }

    public function getCustomFieldsAttribute()
    {
        $hasCustomField = in_array(static::class,setting('custom_field_models',[]));
        if (!$hasCustomField){
            return [];
        }
        $array = $this->customFieldsValues()
            ->join('custom_fields','custom_fields.id','=','custom_field_values.custom_field_id')
            ->where('custom_fields.in_table','=',true)
            ->get()->toArray();

        return convertToAssoc($array,'name');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'notifiable_id', 'id');
    }
    
    public function getFieldObjectAttribute()
    {
        $result = null;
        $this->fieldObject = json_decode($this->data, true);
        if ($this->type == "App\\Notifications\\NewComment") {
            $result = Comment::find($this->fieldObject['comment_id']);
        }
        if ($this->type == "App\\Notifications\\NewFeed") {
            $result = Feed::find($this->fieldObject['feed_id']);;
        }
        if ($this->type == "App\\Notifications\\NewNews") {
            $result = News::find($this->fieldObject['news_id']);;
        }
        if ($this->type == "App\\Notifications\\NewProduct") {
            $result = Product::find($this->fieldObject['product_id']);;
        }
        if ($this->type == "App\\Notifications\\NewMarket") {
            $result = Market::find($this->fieldObject['market_id']);;
        }
        if ($this->type == "App\\Notifications\\NewOrderManager") {
            $result = Order::find($this->fieldObject['order_id']);;
        }
        if ($this->type == "App\\Notifications\\NewOrderAdmin") {
            $result = Order::find($this->fieldObject['order_id']);;
        }
        if ($this->type == "App\\Notifications\\StatusChangedOrder") {
            $result = Order::find($this->fieldObject['order_id']);;
        }
        if ($this->type == "App\\Notifications\\StatusChangedOrderAdmin") {
            $result = Order::find($this->fieldObject['order_id']);;
        }
        if ($this->type == "App\\Notifications\\StatusChangedPayment") {
            $result = Order::find($this->fieldObject['order_id']);;
        }
        if ($this->type == "App\\Notifications\\PaymentConfirmed") {
            $result = Order::find($this->fieldObject['order_id']);;
        }
        if ($this->type == "App\\Notifications\\PaymentConfirmedAdmin") {
            $result = Order::find($this->fieldObject['order_id']);;
        }
        if ($this->type == "App\\Notifications\\StatusCancelOrderAdmin") {
            $result = Order::find($this->fieldObject['order_id']);;
        }
        if ($this->type == "App\\Notifications\\StatusCancelOrder") {
            $result = Order::find($this->fieldObject['order_id']);;
        }
        if ($this->type == "App\\Notifications\\NewLike") {
            $result = Feed::find($this->fieldObject['feed_id']);;
        }
        return $result;
    }
}
