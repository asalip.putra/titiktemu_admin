<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Payment
 * @package App\Models
 * @version August 29, 2019, 9:39 pm UTC
 *
 * @property \App\Models\User user
 * @property double price
 * @property string description
 * @property string status
 * @property string method
 * @property integer user_id
 */
class Message extends Model
{

    public $table = 'messages';
    


    public $fillable = [
        'collection',
        'user_id',
        'receiver_id',
        'market_id',
        'type',
        'last_message'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'collection' => 'string',
        'type' => 'string',
        'user_id' => 'integer',
        'receiver_id' => 'integer',
        'market_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'collection' => 'required',
        'type' => 'required',
        'user_id' => 'required|exists:users,id',
        'receiver_id' => 'required|exists:users,id',
        'market_id' => 'required|exists:markets,id'
    ];
 
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function receiver()
    {
        return $this->belongsTo(\App\Models\User::class, 'receiver_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function market()
    {
        return $this->belongsTo(\App\Models\Market::class, 'market_id', 'id');
    }
}
