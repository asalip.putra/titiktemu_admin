<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Image\Manipulations;
/**
 * Class Comment
 * @package App\Models
 * @version August 29, 2019, 9:39 pm UTC
 *
 * @property \App\Models\User user
 * @property \App\Models\Market market
 * @property string review
 * @property unsignedTinyInteger rate
 * @property integer user_id
 * @property integer market_id
 */
class FeedLike extends Model
{
    public $table = 'feed_likes';

    public $fillable = [
        'user_id',
        'feed_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'integer',
        'feed_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required|exists:users,id',
        'feed_id' => 'required|exists:feeds,id'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function feed()
    {
        return $this->belongsTo(\App\Models\Feed::class, 'feed_id', 'id');
    }
    
}
