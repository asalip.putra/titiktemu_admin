<?php

namespace App\Events;

use App\Models\News;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewsChangedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $newNews;

    public $oldNews;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(News $newNews, News $oldNews)
    {
        //
        $this->newNews = $newNews;
        $this->oldNews = $oldNews;
    }

}
