<?php

namespace App\Events;

use App\Models\Banner;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BannerChangedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $newBanner;

    public $oldBanner;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Banner $newBanner, Banner $oldBanner)
    {
        //
        $this->newBanner = $newBanner;
        $this->oldBanner = $oldBanner;
    }

}
