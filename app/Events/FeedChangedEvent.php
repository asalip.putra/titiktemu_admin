<?php

namespace App\Events;

use App\Models\Feed;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class FeedChangedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $newFeed;

    public $oldFeed;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Feed $newFeed, Feed $oldFeed)
    {
        //
        $this->newFeed = $newFeed;
        $this->oldFeed = $oldFeed;
    }

}
