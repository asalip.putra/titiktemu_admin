<?php

namespace App\Repositories;

use App\Models\Feed;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class FeedRepository
 * @package App\Repositories
 * @version August 29, 2019, 9:39 pm UTC
 *
 * @method Feed findWithoutFail($id, $columns = ['*'])
 * @method Feed find($id, $columns = ['*'])
 * @method Feed first($columns = ['*'])
*/
class FeedRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'description',
        'user_id',
        'type',
        'market_id',
        'product_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Feed::class;
    }
}
