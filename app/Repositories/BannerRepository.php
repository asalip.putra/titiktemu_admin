<?php

namespace App\Repositories;

use App\Models\Banner;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BannerRepository
 * @package App\Repositories
 * @version April 11, 2020, 1:57 pm UTC
 *
 * @method Banner findWithoutFail($id, $columns = ['*'])
 * @method Banner find($id, $columns = ['*'])
 * @method Banner first($columns = ['*'])
*/
class BannerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Banner::class;
    }

    /**
     * get my banner
     */

    public function myBanner()
    {
        return Banner::join("user_banners", "banner_id", "=", "banners.id")
            ->where('user_banners.user_id', auth()->id())->get();
    }

    public function myActiveBanner()
    {
        return Banner::join("user_banners", "banner_id", "=", "banners.id")
            ->where('user_banners.user_id', auth()->id())
            ->where('banners.active','=','1')->get();
    }
}
