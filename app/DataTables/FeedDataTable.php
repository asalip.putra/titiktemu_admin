<?php
/**
 * File name: MarketReviewDataTable.php
 * Last modified: 2020.05.04 at 09:04:19
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\DataTables;

use App\Criteria\Feeds\FeedsOfUserCriteria;
use App\Models\CustomField;
use App\Models\Feed;
use App\Repositories\FeedRepository;
use Barryvdh\DomPDF\Facade as PDF;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Services\DataTable;

/**
 * Class FeedDataTable
 * @package App\DataTables
 */
class FeedDataTable extends DataTable
{
    /**
     * custom fields columns
     * @var array
     */
    public static $customFields = [];

    /**
     * @var FeedRepository
     */
    private $feedRepo;

    private $myFeeds;


    /**
     * FeedDataTable constructor.
     * @param FeedRepository $feedRepo
     */
    public function __construct(FeedRepository $feedRepo)
    {
        $this->feedRepo = $feedRepo;
        $this->myFeeds = $this->feedRepo->getByCriteria(new FeedsOfUserCriteria(auth()->id()))->pluck('id')->toArray();
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        $columns = array_column($this->getColumns(), 'data');
        $dataTable = $dataTable
            ->editColumn('image', function ($feed) {
                return getMediaColumn($feed, 'image');
            })
            ->editColumn('updated_at', function ($feed) {
                return getDateColumn($feed, 'updated_at');
            })->addColumn('action', function ($feed) {
                return view('feeds.datatables_actions', ['id' => $feed->id, 'myFeeds' => $this->myFeeds])->render();
            })
            ->rawColumns(array_merge($columns, ['action']));

        return $dataTable;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Feed $model
     * @return \Illuminate\Database\Eloquent\Builder
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function query(Feed $model)
    {
        // $this->marketReviewRepo->pushCriteria(new OrderMarketReviewsOfUserCriteria(auth()->id()));
        return $this->feedRepo->with("user")->newQuery();

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['title'=>trans('lang.actions'),'width' => '80px', 'printable' => false, 'responsivePriority' => '100'])
            ->parameters(array_merge(
                config('datatables-buttons.parameters'), [
                    'language' => json_decode(
                        file_get_contents(base_path('resources/lang/' . app()->getLocale() . '/datatable.json')
                        ), true)
                ]
            ));
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        $columns = [
            [
                'data' => 'description',
                'title' => trans('lang.feed_description'),

            ],
            [
                'data' => 'image',
                'title' => trans('lang.feed_image'),
                'searchable' => false, 'orderable' => false, 'exportable' => false, 'printable' => false,
            ],
            [
                'data' => 'user.name',
                'title' => trans('lang.feed_users'),

            ],
            [
                'data' => 'updated_at',
                'title' => trans('lang.feed_updated_at'),
                'searchable' => false,
            ]
        ];

        $hasCustomField = in_array(Feed::class, setting('custom_field_models', []));
        if ($hasCustomField) {
            $customFieldsCollection = CustomField::where('custom_field_model', Feed::class)->where('in_table', '=', true)->get();
            foreach ($customFieldsCollection as $key => $field) {
                array_splice($columns, $field->order - 1, 0, [[
                    'data' => 'custom_fields.' . $field->name . '.view',
                    'title' => trans('lang.feed_' . $field->name),
                    'orderable' => false,
                    'searchable' => false,
                ]]);
            }
        }
        return $columns;
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'feedsdatatable_' . time();
    }

    /**
     * Export PDF using DOMPDF
     * @return mixed
     */
    public function pdf()
    {
        $data = $this->getDataForPrint();
        $pdf = PDF::loadView($this->printPreview, compact('data'));
        return $pdf->download($this->filename() . '.pdf');
    }
}