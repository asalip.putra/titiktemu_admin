<?php

namespace App\Criteria\Orders;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class OrdersOfPeriodCriteria.
 *
 * @package namespace App\Criteria\Orders;
 */
class OrdersOfPeriodCriteria implements CriteriaInterface
{
    /**
     * @var array
     */
    private $request;

    /**
     * OrdersOfPeriodCriteria constructor.
     * @param array $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (!isset($this->request)) {
            return $model;
        } else {
            return $model->whereYear('orders.created_at', '=', $this->request['year'])
                  ->whereMonth('orders.created_at', '=', $this->request['month']);
        }
    }
}
