<?php

namespace App\Criteria\Orders;

use Carbon\Carbon;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class OrdersOfDateCriteria.
 *
 * @package namespace App\Criteria\Orders;
 */
class OrdersOfDateCriteria implements CriteriaInterface
{

    /**
     * OrdersOfDateCriteria constructor.
     * @param array $request
     */
    public function __construct()
    {

    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('active', '1')
          ->whereTime('created_at', '<' , Carbon::now()->subHours(2));
    }
}
