<?php

namespace App\Criteria\Feeds;

use App\Models\User;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FeedsfUserCriteria.
 *
 * @package namespace App\Criteria\Feeds;
 */
class FeedsOfUserCriteria implements CriteriaInterface
{
    /**
     * @var User
     */
    private $userId;

    /**
     * FeedsOfUserCriteria constructor.
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (auth()->user()->hasRole('admin')) {
            return $model->select('feeds.*');
        } else if (auth()->user()->hasRole('client')) {
            return $model->where('feeds.user_id', $this->userId)
                ->groupBy('feeds.id')
                ->select('feeds.*');
        }
    }
}
