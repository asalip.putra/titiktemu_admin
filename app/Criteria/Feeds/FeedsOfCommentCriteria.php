<?php
/**
 * File name: FeedsOfCommentCriteria.php
 * Last modified: 2020.04.30 at 08:21:08
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Criteria\Feeds;


use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class FeedsOfCommentCriteria.
 *
 * @package namespace App\Criteria\Products;
 */
class FeedsOfCommentCriteria implements CriteriaInterface
{
    /**
     * @var int
     */
    private $feedId;

    /**
     * FeedsOfCommentCriteria constructor.
     */
    public function __construct($feedId)
    {
        $this->feedId = $feedId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('feed_id', '=', $this->feedId);
    }
}
