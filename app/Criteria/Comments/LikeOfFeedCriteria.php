<?php

namespace App\Criteria\Comments;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class LikesOfFeedCriteria.
 *
 * @package namespace App\Criteria\Likes;
 */
class LikesOfFeedCriteria implements CriteriaInterface
{
    /**
     * @var Feed
     */
    private $feedId;

    /**
     * CommentsOfUserCriteria constructor.
     */
    public function __construct($feedId)
    {
        $this->feedId = $feedId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('feed_id', '=', $this->feedId);
    }
}
