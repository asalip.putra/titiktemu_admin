<?php

namespace App\Criteria\Comments;

use App\Models\User;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class CommentsOfUserCriteria.
 *
 * @package namespace App\Criteria\Feeds;
 */
class CommentsOfUserCriteria implements CriteriaInterface
{
    /**
     * @var User
     */
    private $userId;

    /**
     * FeedsOfUserCriteria constructor.
     */
    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (auth()->user()->hasRole('admin')) {
            return $model->select('comments.*');
        } else if (auth()->user()->hasRole('client')) {
            return $model->where('comments.user_id', $this->userId)
                ->groupBy('comments.id')
                ->select('comments.*');
        }
    }
}
