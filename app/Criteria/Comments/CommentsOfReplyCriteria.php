<?php

namespace App\Criteria\Comments;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class CommentsfUserCriteria.
 *
 * @package namespace App\Criteria\Comments;
 */
class CommentsOfReplyCriteria implements CriteriaInterface
{
    /**
     * @var Comment
     */
    private $commentId;

    /**
     * CommentsOfUserCriteria constructor.
     */
    public function __construct($commentId)
    {
        $this->commentId = $commentId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->where('parent_id', '=', $this->commentId)
        ->where("parent_id", '!=', null);
    }
}
