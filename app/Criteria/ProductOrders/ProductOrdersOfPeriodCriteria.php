<?php

namespace App\Criteria\ProductOrders;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ProductOrdersOfPeriodCriteria.
 *
 * @package namespace App\Criteria\Orders;
 */
class ProductOrdersOfPeriodCriteria implements CriteriaInterface
{
    /**
     * @var array
     */
    private $request;

    /**
     * ProductOrdersOfPeriodCriteria constructor.
     * @param array $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (!isset($this->request)) {
            return $model;
        } else {
            return $model->whereYear('orders.created_at', '=', $this->request['year'])
                ->whereMonth('orders.created_at', '=', $this->request['month']);
        }
    }
}
