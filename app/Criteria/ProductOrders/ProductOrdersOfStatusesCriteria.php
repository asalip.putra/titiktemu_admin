<?php

namespace App\Criteria\ProductOrders;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ProductOrdersOfStatusesCriteria.
 *
 * @package namespace App\Criteria\Orders;
 */
class ProductOrdersOfStatusesCriteria implements CriteriaInterface
{
    /**
     * @var array
     */
    private $statuses;

    /**
     * ProductOrdersOfStatusesCriteria constructor.
     * @param array $request
     */
    public function __construct($statuses)
    {
        $this->statuses = $statuses;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (empty($this->statuses)) {
            return $model;
        } else {
            return $model->whereIn('orders.order_status_id', $this->statuses);
        }
    }
}
