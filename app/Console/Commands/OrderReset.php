<?php

namespace App\Console\Commands;

use App\Criteria\Orders\OrdersOfDateCriteria;
use App\Repositories\MarketRepository;
use Illuminate\Console\Command;
use App\Repositories\OrderRepository;
use App\Repositories\ProductOrderRepository;
use App\Repositories\ProductRepository;
use Carbon\Carbon;

class OrderReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Order Expired';

    /** @var  OrderRepository */
    private $orderRepository;

    /** @var  ProductOrderRepository */
    private $productOrderRepository;

    /**
     * @var MarketRepository
     */
    private $marketRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderRepository $orderRepo, ProductOrderRepository $productOrderRepo, MarketRepository $marketRepo, ProductRepository $productRepo)
    {
        parent::__construct();
        $this->orderRepository = $orderRepo;
        $this->productOrderRepository = $productOrderRepo;
        $this->marketRepository = $marketRepo;
        $this->productRepository = $productRepo;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->orderRepository->pushCriteria(new OrdersOfDateCriteria());
        $orders = $this->orderRepository->orderBy("created_at",'asc')->all();
        $str_order = 'Restock Product ';
        foreach($orders as $order) {
            if ($order->payment->status == 'Waiting for Client') {
                $product_order = $this->productOrderRepository->all()->firstWhere('order_id', '=', $order->id);
                $product = $this->productRepository->findWithoutFail($product_order->product_id);
                $market = $this->marketRepository->findWithoutFail($product['market_id']);
                // if(strtotime($order->created_at->format('H:i:s')) < strtotime($market['limit_payment_hour'])) {
                    $qty = $product->package_items_count + $product_order->quantity;
                    $this->productRepository->update(['package_items_count' => $qty], $product->id);
                    $this->orderRepository->update(['active' => false], $order->id);
                    
                    $str_order .= $product['name'] . ' jumlah ' . $product_order->quantity . ', ';
                // }
            }
        }
        
        $this->comment($str_order);
    }
}
