<?php

namespace App\Http\Controllers;

use App\Criteria\News\NewsOfUserCriteria;
use App\Criteria\Users\AdminsCriteria;
use App\Criteria\Users\ClientsCriteria;
use App\Criteria\Users\ManagersClientsCriteria;
use App\Criteria\Users\ManagersCriteria;
use App\DataTables\NewsDataTable;
use App\Events\NewsChangedEvent;
use App\Http\Requests;
use App\Http\Requests\CreateNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\Repositories\NewsRepository;
use App\Repositories\CustomFieldRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use App\Notifications\NewNews;
use App\Repositories\NotificationRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class NewsController extends Controller
{
    /** @var  NewsRepository */
    private $newsRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    /**
     * @var UploadRepository
    */
    private $uploadRepository;

    /** @var  NotificationRepository */
    private $notificationRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(NewsRepository $newsRepo, CustomFieldRepository $customFieldRepo , UploadRepository $uploadRepo, UserRepository $userRepo, NotificationRepository $notificationRepo)
    {
        parent::__construct();
        $this->newsRepository = $newsRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->uploadRepository = $uploadRepo;
        $this->userRepository = $userRepo;
        $this->notificationRepository = $notificationRepo;
    }

    /**
     * Display a listing of the News.
     *
     * @param NewsDataTable $newsDataTable
     * @return Response
     */
    public function index(NewsDataTable $newsDataTable)
    {
        return $newsDataTable->render('news.index');
    }

    /**
     * Show the form for creating a new News.
     *
     * @return Response
     */
    public function create()
    {
        $this->userRepository->pushCriteria(new AdminsCriteria(auth()->id()));
        $user = $this->userRepository->pluck('name','id');

        $hasCustomField = in_array($this->newsRepository->model(),setting('custom_field_models',[]));
        if($hasCustomField){
            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->newsRepository->model());
            $html = generateCustomField($customFields);
        }
        return view('news.create')->with("customFields", isset($html) ? $html : false)->with("user", $user);
    }

    /**
     * Store a newly created News in storage.
     *
     * @param CreateNewsRequest $request
     *
     * @return Response
     */
    public function store(CreateNewsRequest $request)
    {
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->newsRepository->model());
        try {
            $news = $this->newsRepository->create($input);

            $news->customFieldsValues()->createMany(getCustomFieldsValues($customFields,$request));
            if(isset($input['image']) && $input['image']){
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                $mediaItem = $cacheUpload->getMedia('image')->first();
                $mediaItem->copy($news, 'image');
            }
            
            if (setting('enable_notifications', false)) {
                if (isset($input['title'])) {
                    $this->userRepository->pushCriteria(new ClientsCriteria(auth()->id()));
                    $users = $this->userRepository->all();
                    foreach ($users as $user) {
                        Notification::send([$user], new NewNews($news));
                    }
                }
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully',['operator' => __('lang.news')]));

        return redirect(route('news.index'));
    }

    /**
     * Display the specified News.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->newsRepository->pushCriteria(new NewsOfUserCriteria(auth()->id()));
        $news = $this->newsRepository->findWithoutFail($id);

        if (empty($news)) {
            Flash::error('News not found');

            return redirect(route('news.index'));
        }

        return view('news.show')->with('news', $news);
    }

    /**
     * Show the form for editing the specified News.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $this->newsRepository->pushCriteria(new NewsOfUserCriteria(auth()->id()));
        $news = $this->newsRepository->findWithoutFail($id);

        if (empty($news)) {
            Flash::error(__('lang.not_found', ['operator' => __('lang.news')]));
            return redirect(route('news.index'));
        }
        if($news['active'] == 0){
            $user = $this->userRepository->getByCriteria(new ManagersClientsCriteria())->pluck('name', 'id');
        } else {
            $user = $this->userRepository->getByCriteria(new ManagersCriteria())->pluck('name', 'id');
        }
        $this->userRepository->pushCriteria(new AdminsCriteria(auth()->id()));
        $user = $this->userRepository->pluck('name','id');
        
        $customFieldsValues = $news->customFieldsValues()->with('customField')->get();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->newsRepository->model());
        $hasCustomField = in_array($this->newsRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        return view('news.edit')->with('news', $news)->with("customFields", isset($html) ? $html : false)->with("user", $user);
    }

    /**
     * Update the specified News in storage.
     *
     * @param  int              $id
     * @param UpdateNewsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNewsRequest $request)
    {
        $this->newsRepository->pushCriteria(new NewsOfUserCriteria(auth()->id()));
        $oldNews = $this->newsRepository->findWithoutFail($id);

        if (empty($oldNews)) {
            Flash::error('News not found');
            return redirect(route('news.index'));
        }
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->newsRepository->model());
        try {

            $news = $this->newsRepository->update($input, $id);
            if (isset($input['image']) && $input['image']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                $mediaItem = $cacheUpload->getMedia('image')->first();
                $mediaItem->copy($news, 'image');
            }
            foreach (getCustomFieldsValues($customFields, $request) as $value) {
                $news->customFieldsValues()
                    ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
            }
            event(new NewsChangedEvent($news, $oldNews));
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully', ['operator' => __('lang.news')]));

        return redirect(route('news.index'));
    }

    /**
     * Remove the specified News from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->newsRepository->pushCriteria(new NewsOfUserCriteria(auth()->id()));
        $news = $this->newsRepository->findWithoutFail($id);

        if (empty($news)) {
            Flash::error('News not found');

            return redirect(route('news.index'));
        }

        $this->newsRepository->delete($id);

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.news')]));

        return redirect(route('news.index'));
    }

        /**
     * Remove Media of News
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $news = $this->newsRepository->findWithoutFail($input['id']);
        try {
            if($news->hasMedia($input['collection'])){
                $news->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
