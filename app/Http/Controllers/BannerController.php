<?php

namespace App\Http\Controllers;

use App\Criteria\Banners\BannersOfUserCriteria;
use App\Criteria\Users\ManagersClientsCriteria;
use App\Criteria\Users\ManagersCriteria;
use App\DataTables\BannerDataTable;
use App\Events\BannerChangedEvent;
use App\Http\Requests;
use App\Http\Requests\CreateBannerRequest;
use App\Http\Requests\UpdateBannerRequest;
use App\Repositories\BannerRepository;
use App\Repositories\CustomFieldRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class BannerController extends Controller
{
    /** @var  BannerRepository */
    private $bannerRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    /**
     * @var UploadRepository
    */
    private $uploadRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(BannerRepository $bannerRepo, CustomFieldRepository $customFieldRepo , UploadRepository $uploadRepo, UserRepository $userRepo)
    {
        parent::__construct();
        $this->bannerRepository = $bannerRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->uploadRepository = $uploadRepo;
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the Banner.
     *
     * @param BannerDataTable $bannerDataTable
     * @return Response
     */
    public function index(BannerDataTable $bannerDataTable)
    {
        return $bannerDataTable->render('banners.index');
    }

    /**
     * Show the form for creating a new Banner.
     *
     * @return Response
     */
    public function create()
    {
        $user = $this->userRepository->getByCriteria(new ManagersCriteria())->pluck('name', 'id');
        $usersSelected = [];
        $hasCustomField = in_array($this->bannerRepository->model(),setting('custom_field_models',[]));
        if($hasCustomField){
            $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->bannerRepository->model());
            $html = generateCustomField($customFields);
        }
        return view('banners.create')->with("customFields", isset($html) ? $html : false)->with("user", $user)->with("usersSelected", $usersSelected);
    }

    /**
     * Store a newly created Banner in storage.
     *
     * @param CreateBannerRequest $request
     *
     * @return Response
     */
    public function store(CreateBannerRequest $request)
    {
        $input = $request->all();
        if (auth()->user()->hasRole(['admin', 'manager','client'])) {
            $input['users'] = [auth()->id()];
        }
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->bannerRepository->model());
        try {
            $banner = $this->bannerRepository->create($input);
            $banner->customFieldsValues()->createMany(getCustomFieldsValues($customFields,$request));
            if(isset($input['image']) && $input['image']){
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                $mediaItem = $cacheUpload->getMedia('image')->first();
                $mediaItem->copy($banner, 'image');
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully',['operator' => __('lang.banner')]));

        return redirect(route('banners.index'));
    }

    /**
     * Display the specified Banner.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $this->bannerRepository->pushCriteria(new BannersOfUserCriteria(auth()->id()));
        $banner = $this->bannerRepository->findWithoutFail($id);

        if (empty($banner)) {
            Flash::error('Banner not found');

            return redirect(route('banners.index'));
        }

        return view('banners.show')->with('banner', $banner);
    }

    /**
     * Show the form for editing the specified Banner.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        // $this->bannerRepository->pushCriteria(new BannersOfUserCriteria(auth()->id()));
        $banner = $this->bannerRepository->findWithoutFail($id);

        if (empty($banner)) {
            Flash::error(__('lang.not_found', ['operator' => __('lang.banner')]));
            return redirect(route('banners.index'));
        }
        if($banner['active'] == 0){
            $user = $this->userRepository->getByCriteria(new ManagersClientsCriteria())->pluck('name', 'id');
        } else {
            $user = $this->userRepository->getByCriteria(new ManagersCriteria())->pluck('name', 'id');
        }

        $usersSelected = $banner->users()->pluck('users.id')->toArray();
        
        $customFieldsValues = $banner->customFieldsValues()->with('customField')->get();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->bannerRepository->model());
        $hasCustomField = in_array($this->bannerRepository->model(), setting('custom_field_models', []));
        if ($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        return view('banners.edit')->with('banner', $banner)->with("customFields", isset($html) ? $html : false)->with("user", $user)->with("usersSelected", $usersSelected);
    }

    /**
     * Update the specified Banner in storage.
     *
     * @param  int              $id
     * @param UpdateBannerRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBannerRequest $request)
    {
        $this->bannerRepository->pushCriteria(new BannersOfUserCriteria(auth()->id()));
        $oldBanner = $this->bannerRepository->findWithoutFail($id);

        if (empty($oldBanner)) {
            Flash::error('Banner not found');
            return redirect(route('banners.index'));
        }
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->bannerRepository->model());
        try {

            $banner = $this->bannerRepository->update($input, $id);
            if (isset($input['image']) && $input['image']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                $mediaItem = $cacheUpload->getMedia('image')->first();
                $mediaItem->copy($banner, 'image');
            }
            foreach (getCustomFieldsValues($customFields, $request) as $value) {
                $banner->customFieldsValues()
                    ->updateOrCreate(['custom_field_id' => $value['custom_field_id']], $value);
            }
            event(new BannerChangedEvent($banner, $oldBanner));
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully', ['operator' => __('lang.banner')]));

        return redirect(route('banners.index'));
    }

    /**
     * Remove the specified Banner from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $this->bannerRepository->pushCriteria(new BannersOfUserCriteria(auth()->id()));
        $banner = $this->bannerRepository->findWithoutFail($id);

        if (empty($banner)) {
            Flash::error('Banner not found');

            return redirect(route('banners.index'));
        }

        $this->bannerRepository->delete($id);

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.banner')]));

        return redirect(route('banners.index'));
    }

        /**
     * Remove Media of Banner
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $banner = $this->bannerRepository->findWithoutFail($input['id']);
        try {
            if($banner->hasMedia($input['collection'])){
                $banner->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
