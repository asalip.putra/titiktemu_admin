<?php


/**
 * File name: BCAController.php
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers;

use App\Models\DeliveryAddress;
use App\Models\Payment;
use App\Models\User;
use App\Repositories\DeliveryAddressRepository;
use Illuminate\Http\Request;
use Flash;

class MandiriController extends ParentOrderController
{

    /**
     * @var Api
     */
    private $api;
    private $currency;
    /** @var DeliveryAddressRepository
     *
     */
    private $deliveryAddressRepo;

    public function __init()
    {
        // $this->api = new Api(
        //     config('services.razorpay.key'),
        //     config('services.razorpay.secret')
        // );
        // $this->currency = setting('default_currency_code', 'INR');
        // $this->deliveryAddressRepo = new DeliveryAddressRepository(app());
    }


    public function index()
    {
        return $this->sendResponse('Mandiri', 'Market retrieved successfully');
    }
}
