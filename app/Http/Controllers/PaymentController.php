<?php

namespace App\Http\Controllers;

use App\DataTables\PaymentDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePaymentRequest;
use App\Http\Requests\UpdatePaymentRequest;
use Illuminate\Support\Facades\Notification;
use App\Notifications\StatusChangedPayment;
use App\Repositories\PaymentRepository;
use App\Repositories\CustomFieldRepository;
use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use App\Repositories\UploadRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class PaymentController extends Controller
{
    /** @var  PaymentRepository */
    private $paymentRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    /**
     * @var UploadRepository
    */
    private $uploadRepository;

    /**
    * @var UserRepository
    */
    private $userRepository;

    /**
     * @var OrderRepository
     */
    private $orderRepository;

    public function __construct(PaymentRepository $paymentRepo, UploadRepository $uploadRepo, CustomFieldRepository $customFieldRepo , UserRepository $userRepo, OrderRepository $orderRepo)
    {
        parent::__construct();
        $this->paymentRepository = $paymentRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->userRepository = $userRepo;
        $this->uploadRepository = $uploadRepo;
        $this->orderRepository = $orderRepo;
    }

    /**
     * Display a listing of the Payment.
     *
     * @param PaymentDataTable $paymentDataTable
     * @return Response
     */
    public function index(PaymentDataTable $paymentDataTable)
    {
        return $paymentDataTable->render('payments.index');
    }

    /**
     * Show the form for creating a new Payment.
     *
     * @return Response
     */
    public function create()
    {
        $user = $this->userRepository->pluck('name','id');
        
        $hasCustomField = in_array($this->paymentRepository->model(),setting('custom_field_models',[]));
            if($hasCustomField){
                $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->paymentRepository->model());
                $html = generateCustomField($customFields);
            }
        return view('payments.create')->with("customFields", isset($html) ? $html : false)->with("user",$user);
    }

    /**
     * Store a newly created Payment in storage.
     *
     * @param CreatePaymentRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentRequest $request)
    {
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->paymentRepository->model());
        try {
            $payment = $this->paymentRepository->create($input);
            $payment->customFieldsValues()->createMany(getCustomFieldsValues($customFields,$request));
            
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully',['operator' => __('lang.payment')]));

        return redirect(route('payments.index'));
    }

    /**
     * Display the specified Payment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $payment = $this->paymentRepository->findWithoutFail($id);

        $order = $this->orderRepository->findByField('payment_id', $payment['id'])->first();

        $user = $this->userRepository->findByField('id', $payment['user_id'])->first();

        $customFieldsValues = $payment->customFieldsValues()->with('customField')->get();
        $customFields = isset($customFieldsValues) ? $customFieldsValues : null;

        if (!$order) {
            Flash::error('Order not found');

            return redirect(route('payments.index'));
        }

        if (!$user) {
            Flash::error('User not found');

            return redirect(route('payments.index'));
        }

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect(route('payments.index'));
        }

        return view('payments.show')->with('payment', $payment)->with('order', $order)->with('user', $user)->with("customFields", $customFields);
    }

    /**
     * Show the form for editing the specified Payment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $payment = $this->paymentRepository->findWithoutFail($id);
        $user = $this->userRepository->pluck('name','id');
        

        if (empty($payment)) {
            Flash::error(__('lang.not_found',['operator' => __('lang.payment')]));

            return redirect(route('payments.index'));
        }
        $customFieldsValues = $payment->customFieldsValues()->with('customField')->get();
        $customFields =  $this->customFieldRepository->findByField('custom_field_model', $this->paymentRepository->model());
        $hasCustomField = in_array($this->paymentRepository->model(),setting('custom_field_models',[]));
        if($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        return view('payments.edit')->with('payment', $payment)->with("customFields", isset($html) ? $html : false)->with("user",$user);
    }

    /**
     * Update the specified Payment in storage.
     *
     * @param  int              $id
     * @param UpdatePaymentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentRequest $request)
    {
        $payment = $this->paymentRepository->findWithoutFail($id);
        // $oldStatus = $payment->status;
        if (empty($payment)) {
            Flash::error('Payment not found');
            return redirect(route('payments.index'));
        }
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->paymentRepository->model());
        try {
            $payment = $this->paymentRepository->update($input, $id);
            
            // if (setting('enable_notifications', false)) {
            //     $user = $this->userRepository->findWithoutFail($payment->user_id);
            //     if (isset($input['status']) && $input['status'] != $oldStatus) {
            //         Notification::send([$user], new StatusChangedPayment($payment));
            //     }
            // }

            if (isset($input['image']) && $input['image']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                $mediaItem = $cacheUpload->getMedia('image')->first();
                $mediaItem->copy($payment, 'image');
            }
            foreach (getCustomFieldsValues($customFields, $request) as $value){
                $payment->customFieldsValues()
                    ->updateOrCreate(['custom_field_id'=>$value['custom_field_id']],$value);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully',['operator' => __('lang.payment')]));

        return redirect(route('payments.index'));
    }

    /**
     * Remove the specified Payment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $payment = $this->paymentRepository->findWithoutFail($id);

        if (empty($payment)) {
            Flash::error('Payment not found');

            return redirect(route('payments.index'));
        }

        $this->paymentRepository->delete($id);

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.payment')]));

        return redirect(route('payments.index'));
    }

        /**
     * Remove Media of Payment
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $payment = $this->paymentRepository->findWithoutFail($input['id']);
        try {
            if($payment->hasMedia($input['collection'])){
                $payment->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
