<?php

namespace App\Http\Controllers;

use App\Criteria\Feeds\FeedsOfUserCriteria;
use App\DataTables\FeedDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateFeedRequest;
use App\Http\Requests\UpdateFeedRequest;
use App\Repositories\FeedRepository;
use App\Repositories\CustomFieldRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class FeedController extends Controller
{
    /** @var  FeedRepository */
    private $feedRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    /**
     * @var UploadRepository
    */
    private $uploadRepository;

    /**
    * @var UserRepository
    */
    private $userRepository;

    public function __construct(FeedRepository $feedRepo, CustomFieldRepository $customFieldRepo , UserRepository $userRepo, UploadRepository $uploadRepo)
    {
        parent::__construct();
        $this->feedRepository = $feedRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->userRepository = $userRepo;
        $this->uploadRepository = $uploadRepo;
    }

    /**
     * Display a listing of the Feed.
     *
     * @param FeedDataTable $feedDataTable
     * @return Response
     */
    public function index(FeedDataTable $feedDataTable)
    {
        return $feedDataTable->render('feeds.index');
    }

    /**
     * Show the form for creating a new Feed.
     *
     * @return Response
     */
    public function create()
    {
        $user = $this->userRepository->pluck('name','id');
        
        $hasCustomField = in_array($this->feedRepository->model(),setting('custom_field_models',[]));
            if($hasCustomField){
                $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->feedRepository->model());
                $html = generateCustomField($customFields);
            }
        return view('feeds.create')->with("customFields", isset($html) ? $html : false)->with("user",$user);
    }

    /**
     * Store a newly created Feed in storage.
     *
     * @param CreateFeedRequest $request
     *
     * @return Response
     */
    public function store(CreateFeedRequest $request)
    {
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->feedRepository->model());
        try {
            $feed = $this->feedRepository->create($input);
            $feed->customFieldsValues()->createMany(getCustomFieldsValues($customFields,$request));
            if(isset($input['image']) && $input['image']){
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                $mediaItem = $cacheUpload->getMedia('image')->first();
                $mediaItem->copy($feed, 'image');
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.saved_successfully',['operator' => __('lang.feed')]));

        return redirect(route('feeds.index'));
    }

    /**
     * Display the specified Feed.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function show($id)
    {
        $this->feedRepository->pushCriteria(new FeedsOfUserCriteria(auth()->id()));
        $feed = $this->feedRepository->findWithoutFail($id);

        if (empty($feed)) {
            Flash::error('Feed not found');

            return redirect(route('feeds.index'));
        }

        return view('feeds.show')->with('feed', $feed);
    }

    /**
     * Show the form for editing the specified Feed.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function edit($id)
    {
        $this->feedRepository->pushCriteria(new FeedsOfUserCriteria(auth()->id()));
        $feed = $this->feedRepository->findWithoutFail($id);
        if (empty($feed)) {
            Flash::error(__('lang.not_found',['operator' => __('lang.feed')]));

            return redirect(route('feeds.index'));
        }
        $user = $this->userRepository->pluck('name','id');

        $customFieldsValues = $feed->customFieldsValues()->with('customField')->get();
        $customFields =  $this->customFieldRepository->findByField('custom_field_model', $this->feedRepository->model());
        $hasCustomField = in_array($this->feedRepository->model(),setting('custom_field_models',[]));
        if($hasCustomField) {
            $html = generateCustomField($customFields, $customFieldsValues);
        }

        return view('feeds.edit')->with('feed', $feed)->with("customFields", isset($html) ? $html : false)->with("user",$user);
    }

    /**
     * Update the specified Feed in storage.
     *
     * @param int $id
     * @param UpdateFeedRequest $request
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function update($id, UpdateFeedRequest $request)
    {
        $this->feedRepository->pushCriteria(new FeedsOfUserCriteria(auth()->id()));
        $feed = $this->feedRepository->findWithoutFail($id);

        if (empty($feed)) {
            Flash::error('Feed not found');
            return redirect(route('feeds.index'));
        }
        $input = $request->all();
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->feedRepository->model());
        try {
            $feed = $this->feedRepository->update($input, $id);
            if (isset($input['image']) && $input['image']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                $mediaItem = $cacheUpload->getMedia('image')->first();
                $mediaItem->copy($feed, 'image');
            }
            
            foreach (getCustomFieldsValues($customFields, $request) as $value){
                $feed->customFieldsValues()
                    ->updateOrCreate(['custom_field_id'=>$value['custom_field_id']],$value);
            }
        } catch (ValidatorException $e) {
            Flash::error($e->getMessage());
        }

        Flash::success(__('lang.updated_successfully',['operator' => __('lang.feed')]));

        return redirect(route('feeds.index'));
    }

    /**
     * Remove the specified Feed from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function destroy($id)
    {
        $this->feedRepository->pushCriteria(new FeedsOfUserCriteria(auth()->id()));
        $feed = $this->feedRepository->findWithoutFail($id);

        if (empty($feed)) {
            Flash::error('Feed not found');

            return redirect(route('feeds.index'));
        }

        $this->feedRepository->delete($id);

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.feed')]));

        return redirect(route('feeds.index'));
    }

        /**
     * Remove Media of Feed
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $feed = $this->feedRepository->findWithoutFail($input['id']);
        try {
            if($feed->hasMedia($input['collection'])){
                $feed->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
