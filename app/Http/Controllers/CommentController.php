<?php

namespace App\Http\Controllers;

use App\Criteria\Comments\CommentsOfUserCriteria;
use App\DataTables\CommentDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateFeedRequest;
use App\Http\Requests\UpdateFeedRequest;
use App\Repositories\CommentRepository;
use App\Repositories\CustomFieldRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Prettus\Validator\Exceptions\ValidatorException;

class CommentController extends Controller
{
    /** @var  CommentRepository */
    private $commentRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    /**
     * @var UploadRepository
    */
    private $uploadRepository;

    /**
    * @var UserRepository
    */
    private $userRepository;

    public function __construct(CommentRepository $commentRepo, CustomFieldRepository $customFieldRepo , UserRepository $userRepo, UploadRepository $uploadRepo)
    {
        parent::__construct();
        $this->commentRepository = $commentRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->userRepository = $userRepo;
        $this->uploadRepository = $uploadRepo;
    }

    /**
     * Display a listing of the Feed.
     *
     * @param CommentDataTable $commentDataTable
     * @return Response
     */
    public function index(CommentDataTable $commentDataTable)
    {
        return $commentDataTable->render('comments.index');
    }

    /**
     * Display the specified Feed.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function show($id)
    {
        $this->commentRepository->pushCriteria(new CommentsOfUserCriteria(auth()->id()));
        $comment = $this->commentRepository->findWithoutFail($id);

        if (empty($comment)) {
            Flash::error('Feed not found');

            return redirect(route('comments.index'));
        }

        return view('comments.show')->with('comment', $comment);
    }
    /**
     * Remove the specified Feed from storage.
     *
     * @param int $id
     *
     * @return Response
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function destroy($id)
    {
        $this->commentRepository->pushCriteria(new CommentsOfUserCriteria(auth()->id()));
        $comment = $this->commentRepository->findWithoutFail($id);

        if (empty($comment)) {
            Flash::error('Comment not found');

            return redirect(route('comments.index'));
        }

        $this->commentRepository->delete($id);

        Flash::success(__('lang.deleted_successfully',['operator' => __('lang.comment')]));

        return redirect(route('comments.index'));
    }

        /**
     * Remove Media of Feed
     * @param Request $request
     */
    public function removeMedia(Request $request)
    {
        $input = $request->all();
        $comment = $this->commentRepository->findWithoutFail($input['id']);
        try {
            if($comment->hasMedia($input['collection'])){
                $comment->getFirstMedia($input['collection'])->delete();
            }
        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }
    }
}
