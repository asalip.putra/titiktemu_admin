<?php
/**
 * File name: MessageAPIController.php
 * Last modified: 2020.05.04 at 09:04:18
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Notifications\NewMessage;
use App\Models\Message;
use App\Repositories\MessageRepository;
use App\Repositories\UserRepository;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class FeedController
 * @package App\Http\Controllers\API
 */
class MessageAPIController extends Controller
{
    /** @var  MessageRepository */
    private $messageRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(MessageRepository $messageRepo, UserRepository $userRepo)
    {
        $this->messageRepository = $messageRepo;
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the News.
     * GET|HEAD /News
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            $this->messageRepository->pushCriteria(new RequestCriteria($request));
            $this->messageRepository->pushCriteria(new LimitOffsetCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $message = $this->messageRepository->all();

        return $this->sendResponse($message->toArray(), 'Message retrieved successfully');
    }

    /**
     * Display the specified News.
     * GET|HEAD /news/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        /** @var News $news */
        if (!empty($this->messageRepository)) {
            $this->messageRepository->pushCriteria(new RequestCriteria($request));
            $message = $this->messageRepository->findWithoutFail($id);
        }

        if (empty($message)) {
            return $this->sendError('Message not found');
        }

        return $this->sendResponse($message->toArray(), 'Message retrieved successfully');
    }

    /**
     * Store a newly created Market in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();

        try {
            $message = $this->messageRepository->create($input);

        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($message->toArray(), 'Message Saved successfully');
    }

    public function push(Request $request)
    {
        $input = $request->all();

        try {
            // $message = $this->messageRepository->create($input);

            if (setting('enable_notifications', false)) {
                if (isset($input["message"])) {
                    $receiver = $this->userRepository->findWithoutFail($input["receiver"]);
                    $user = $this->userRepository->findWithoutFail($input["sender"]);
                    if (!empty($receiver)) {
                        Notification::send([$receiver], new NewMessage($user, $input["message"]));
                    }
                }
            }

        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($input, 'Message Saved successfully');
    }

    /**
     * Update the specified Order in storage.
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $message = $this->messageRepository->findWithoutFail($id);

        if (empty($message)) {
            return $this->sendError('Message not found');
        }
        $input = $request->all();

        /** @var News $news */
        try {
            $message = $this->messageRepository->update($input, $id);

            if (setting('enable_notifications', false)) {
                if (isset($message)) {
                    $receiver = $this->userRepository->findWithoutFail($message->receiver_id);
                    $user = $this->userRepository->findWithoutFail($message->user_id);
                    if (!empty($receiver)) {
                        // Notification::send([$receiver], new NewMessage($message, $user));
                    }
                }
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($message->toArray(), 'Message retrieved successfully');
    }
}
