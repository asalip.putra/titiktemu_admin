<?php
/**
 * File name: NewsAPIController.php
 * Last modified: 2020.05.04 at 09:04:18
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Models\Comment;
use App\Repositories\CommentRepository;
use App\Repositories\FeedRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use App\Repositories\NotificationRepository;
use Illuminate\Support\Facades\Notification;
use Flash;
use Illuminate\Http\Request;
use App\Notifications\NewComment;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use App\Criteria\Comments\CommentsOfFeedCriteria;
use App\Criteria\Comments\CommentsOfReplyCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class CommentController
 * @package App\Http\Controllers\API
 */
class CommentAPIController extends Controller
{
    /** @var  FeedRepository */
    private $feedRepository;

    /** @var  CommentRepository */
    private $commentRepository;

    /** @var  UploadRepository */
    private $uploadRepository;

    /** @var  NotificationRepository */
    private $notificationRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(CommentRepository $commentRepo, UploadRepository $uploadRepo, FeedRepository $feedRepo, NotificationRepository $notificationRepo, UserRepository $userRepo)
    {
        $this->commentRepository = $commentRepo;
        $this->uploadRepository = $uploadRepo;
        $this->feedRepository = $feedRepo;
        $this->notificationRepository = $notificationRepo;
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the News.
     * GET|HEAD /News
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            $this->commentRepository->pushCriteria(new RequestCriteria($request));
            $this->commentRepository->pushCriteria(new LimitOffsetCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $comment = $this->commentRepository->all();

        return $this->sendResponse($comment->toArray(), 'Comment retrieved successfully');
    }

    /**
     * Display the specified News.
     * GET|HEAD /news/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        /** @var News $news */
        if (!empty($this->commentRepository)) {
            $this->commentRepository->pushCriteria(new RequestCriteria($request));
            $comment = $this->commentRepository->findWithoutFail($id);
        }

        if (empty($comment)) {
            return $this->sendError('Comment not found');
        }

        return $this->sendResponse($comment->toArray(), 'Comment retrieved successfully');
    }

    public function feed(Request $request, $id)
    {
        try{
            $this->commentRepository->pushCriteria(new RequestCriteria($request));
            $this->commentRepository->pushCriteria(new LimitOffsetCriteria($request));
            $this->commentRepository->pushCriteria(new CommentsOfFeedCriteria($id));

            $comment = $this->commentRepository->all();

        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($comment->toArray(), 'Comments retrieved successfully');
    }

    public function reply(Request $request, $id)
    {
        try{
            $this->commentRepository->pushCriteria(new RequestCriteria($request));
            $this->commentRepository->pushCriteria(new LimitOffsetCriteria($request));
            $this->commentRepository->pushCriteria(new CommentsOfReplyCriteria($id));

            $comment = $this->commentRepository->all();

        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($comment->toArray(), 'Comments retrieved successfully');
    }

     /**
     * Store a newly created Product in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        try {
            $comment = $this->commentRepository->create($input);
            if (isset($input['image']) && $input['image']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                $mediaItem = $cacheUpload->getMedia('image')->first();
                $mediaItem->copy($comment, 'image');
            }

            if (setting('enable_notifications', false)) {
                if (isset($input['description'])) {
                    $feed = $this->feedRepository->findWithoutFail($input['feed_id']);
                    $user = $this->userRepository->findWithoutFail($feed->user_id);
                    Notification::send([$user], new NewComment($comment));
                }
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($comment->toArray(), __('lang.saved_successfully', ['operator' => __('lang.product')]));
    }

    /**
     * Remove the specified Favorite from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $comment = $this->commentRepository->findWithoutFail($id);

        if (empty($comment)) {
            return $this->sendError('Feed not found');

        }

        $this->commentRepository->delete($id);

        return $this->sendResponse($comment, __('lang.deleted_successfully',['operator' => __('lang.favorite')]));

    }
}
