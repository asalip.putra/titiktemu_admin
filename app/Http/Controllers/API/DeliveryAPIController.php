<?php

namespace App\Http\Controllers\API;


use App\Http\Requests\UpdateFavoriteRequest;
use App\Models\City;
use App\Repositories\CityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class FavoriteController
 * @package App\Http\Controllers\API
 */

class DeliveryAPIController extends Controller
{
    /** @var  CityRepository */
    private $cityRepository;

    public function __construct(CityRepository $cityRepository)
    {
        $this->cityRepository = $cityRepository;
    }

    /**
     * Display a listing of the Favorite.
     * GET|HEAD /favorites
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
      // $cities = $this->cityRepository->all();
      // return $this->sendResponse($cities->toArray(), 'Cities retrieved successfully');
    }

    public function cities(Request $request)
    {
      try{
        $this->cityRepository->pushCriteria(new RequestCriteria($request));
        $this->cityRepository->pushCriteria(new LimitOffsetCriteria($request));
    } catch (RepositoryException $e) {
        return $this->sendError($e->getMessage());
    }
    $cities = $this->cityRepository->all();

    return $this->sendResponse($cities->toArray(), 'Cities retrieved successfully');
    }

    public function city(Request $request, $id)
    {
        /** @var Banner $banner */
        if (!empty($this->cityRepository)) {
          $city = $this->cityRepository->findWithoutFail($id);
      }

      if (empty($city)) {
          return $this->sendError('City not found');
      }

      return $this->sendResponse($city->toArray(), 'City retrieved successfully');
    }

    /**
     * Display a listing of the Cart.
     * GET|HEAD /carts
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function costs(Request $request)
    {
        $input = $request->all();

        $delivery = getDeliveryFee($input['origin'], $input['destination'], $input['weight']);
        
        $couriers = array();
        foreach ($delivery as $courier) {
            if(!empty($courier['costs'])){
                foreach ($courier['costs'] as $cost) {
                    $cost['code'] = $courier['code'];
                    $cost['name'] = $courier['name'];
                    $couriers[] = $cost;
                }
            }
        }
        return $this->sendResponse($couriers, 'Costs retrieved successfully');
    }

    /**
     * Display the specified Favorite.
     * GET|HEAD /favorites/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var Favorite $favorite */
        if (!empty($this->favoriteRepository)) {
            $favorite = $this->favoriteRepository->findWithoutFail($id);
        }

        if (empty($favorite)) {
            return $this->sendError('Favorite not found');
        }

        return $this->sendResponse($favorite->toArray(), 'Favorite retrieved successfully');
    }

}
