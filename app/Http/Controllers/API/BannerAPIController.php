<?php
/**
 * File name: BannerAPIController.php
 * Last modified: 2020.05.04 at 09:04:18
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers\API;


use App\Criteria\Banners\ActiveCriteria;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Repositories\BannerRepository;
use Flash;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class BannerController
 * @package App\Http\Controllers\API
 */
class BannerAPIController extends Controller
{
    /** @var  BannerRepository */
    private $bannerRepository;

    public function __construct(BannerRepository $bannerRepo)
    {
        $this->bannerRepository = $bannerRepo;
    }

    /**
     * Display a listing of the Banner.
     * GET|HEAD /Banner
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            $this->bannerRepository->pushCriteria(new RequestCriteria($request));
            $this->bannerRepository->pushCriteria(new LimitOffsetCriteria($request));
            $this->bannerRepository->pushCriteria(new ActiveCriteria());
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $banner = $this->bannerRepository->all();

        return $this->sendResponse($banner->toArray(), 'Banner retrieved successfully');
    }

    /**
     * Display the specified Banner.
     * GET|HEAD /banner/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var Banner $banner */
        if (!empty($this->bannerRepository)) {
            $banner = $this->bannerRepository->findWithoutFail($id);
        }

        if (empty($banner)) {
            return $this->sendError('Banner not found');
        }

        return $this->sendResponse($banner->toArray(), 'Banner retrieved successfully');
    }
}
