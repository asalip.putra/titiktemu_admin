<?php

namespace App\Http\Controllers\API;

use App\Criteria\Orders\OrdersOfUserCriteria;
use App\Models\Payment;
use App\Repositories\PaymentRepository;
use App\Criteria\Users\AdminsCriteria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\PaymentConfirmed;
use App\Notifications\PaymentConfirmedAdmin;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CustomFieldRepository;
use App\Repositories\OrderRepository;
use App\Repositories\ProductRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Notification;
use Prettus\Repository\Exceptions\RepositoryException;
use Flash;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class PaymentController
 * @package App\Http\Controllers\API
 */
class PaymentAPIController extends Controller
{
    /** @var  PaymentRepository */
    private $paymentRepository;

    /** @var  OrderRepository */
    private $orderRepository;

    /** @var  UserRepository */
    private $userRepository;

    /**
     * @var CustomFieldRepository
     */
    private $customFieldRepository;

    private $productRepository;

    public function __construct(PaymentRepository $paymentRepo, CustomFieldRepository $customFieldRepo, OrderRepository $orderRepository, UserRepository $userRepository, ProductRepository $productRepository)
    {
        $this->paymentRepository = $paymentRepo;
        $this->customFieldRepository = $customFieldRepo;
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the Payment.
     * GET|HEAD /payments
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $this->paymentRepository->pushCriteria(new RequestCriteria($request));
            $this->paymentRepository->pushCriteria(new LimitOffsetCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $payments = $this->paymentRepository->all();

        return $this->sendResponse($payments->toArray(), 'Payments retrieved successfully');
    }

    /**
     * Display the specified Payment.
     * GET|HEAD /payments/{id}
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        /** @var Payment $payment */
        if (!empty($this->paymentRepository)) {
            $payment = $this->paymentRepository->findWithoutFail($id);
        }

        if (empty($payment)) {
            return $this->sendError('Payment not found');
        }

        return $this->sendResponse($payment->toArray(), 'Payment retrieved successfully');
    }

    public function byMonth()
    {
        $payments = [];
        if (!empty($this->paymentRepository)) {
            $payments = $this->paymentRepository->orderBy("created_at",'asc')->all()->map(function ($row) {
                $row['month'] = $row['created_at']->format('M');
                return $row;
            })->groupBy('month')->map(function ($row) {
                return $row->sum('price');
            });
        }
        return $this->sendResponse([array_values($payments->toArray()),array_keys($payments->toArray())], 'Payment retrieved successfully');
    }

    /**
     * Update the specified Order in storage.
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $payment = $this->paymentRepository->findWithoutFail($id);
        $oldStatus = $payment->status;
        if (empty($payment)) {
            return $this->sendError('Payment not found');
        }
        
        $input = $request->all();
        // if (isset($input['status']) && $input['status'] == "Confirmed") {
        //     $order = $this->orderRepository->findByField('payment_id' , $id);
        //     $order = $this->orderRepository->findWithoutFail($order[0]->id);
        //     $sale_qty = 0;
        //     foreach ($order->productOrders as $productOrder) {
        //         $product = $this->productRepository->findWithoutFail($productOrder->product_id);
        //         $sale_qty += $productOrder->quantity;
        //         $qty = $product->package_items_count - $sale_qty;
        //         $this->productRepository->update(['package_items_count' => $qty], $productOrder->product_id);
        //         $sale_qty = 0;
        //     }
        // }
        
        $customFields = $this->customFieldRepository->findByField('custom_field_model', $this->paymentRepository->model());
        try {
            $payment = $this->paymentRepository->update($input, $id);
            
            if (setting('enable_notifications', false)) {
                $this->userRepository->pushCriteria(new AdminsCriteria(auth()->id()));
                $admin = $this->userRepository->all();
                $order = $this->orderRepository->findByField('payment_id', $payment->id);

                // print_r($admin);exit();
                if (isset($input['status']) && $input['status'] == "Confirmed") {
                    Notification::send([$order[0]->productOrders[0]->product->market->users[0]], new PaymentConfirmed($order[0]));
                    Notification::send([$admin[0]], new PaymentConfirmedAdmin($order[0]));
                }
            }

            if (isset($input['image']) && $input['image']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                $mediaItem = $cacheUpload->getMedia('image')->first();
                $mediaItem->copy($payment, 'image');
            }
            foreach (getCustomFieldsValues($customFields, $request) as $value){
                $payment->customFieldsValues()
                    ->updateOrCreate(['custom_field_id'=>$value['custom_field_id']],$value);
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($payment->toArray(), __('lang.saved_successfully', ['operator' => __('lang.payment')]));
    }
}
