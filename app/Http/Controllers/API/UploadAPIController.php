<?php
/**
 * File name: UserAPIController.php
 * Last modified: 2020.05.04 at 09:04:09
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\UploadRequest;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use App\Repositories\MarketRepository;
use App\Repositories\ProductRepository;
use App\Repositories\PaymentRepository;
use Prettus\Validator\Exceptions\ValidatorException;

class UploadAPIController extends Controller
{
    private $userRepository;
    private $uploadRepository;
    private $marketRepository;
    private $productRepository;
    private $paymentRepository;
    private $roleRepository;
    private $customFieldRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $userRepository, UploadRepository $uploadRepository, MarketRepository $marketRepository, ProductRepository $productRepository, PaymentRepository $paymentRepository)
    {
        $this->userRepository = $userRepository;
        $this->uploadRepository = $uploadRepository;
        $this->marketRepository = $marketRepository;
        $this->productRepository = $productRepository;
        $this->paymentRepository = $paymentRepository;
    }
    
    /**
     * @param UploadRequest $request
     */
    public function store(UploadRequest $request)
    {
        $input = $request->all();
        try {
            $upload = $this->uploadRepository->create($input);
            $upload->addMedia($input['file'])
                ->withCustomProperties(['uuid' => $input['uuid'], 'user_id' => auth()->id()])
                ->toMediaCollection($input['field']);
        } catch (ValidatorException $e) {
            return $this->sendResponse(false, $e->getMessage());
        }
    }

    /**
     * Display the specified News.
     * GET|HEAD /news/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $input = $request->all();

        $cacheUpload = $this->uploadRepository->getByUuid($input['uuid']);

        if (!empty($input['existing_id'])) {
            $result = $this->uploadRepository->clear($input['existing_id']);
        }

        $mediaItem = $cacheUpload->getMedia('image')->first();

        return $this->sendResponse($mediaItem, 'News retrieved successfully');
    }

    /**
     * clear cache from Upload table
     */
    public function update(UploadRequest $request)
    {
        $input = $request->all();

        if($input['model'] == 'user') {
            $model = $this->userRepository->findWithoutFail(auth()->id());
        }

        if($input['model'] == 'market') {
            $model = $this->marketRepository->findWithoutFail($input['model_id']);
        }

        if($input['model'] == 'product') {
            $model = $this->productRepository->findWithoutFail($input['model_id']);
        }

        if($input['model'] == 'payment') {
            $model = $this->paymentRepository->findWithoutFail($input['model_id']);
        }

        try {
            $cacheUpload = $this->uploadRepository->getByUuid($input['uuid']);
            $mediaItem = $cacheUpload->getMedia($input['field'])->first();
            $mediaItem->copy($model, $input['field']);
            return $this->sendResponse($model, 'Media update successfully');
        } catch (ValidatorException $e) {
            return $this->sendResponse(false, $e->getMessage());
        }
    }

    /**
     * clear cache from Upload table
     */
    public function clear(UploadRequest $request)
    {
        $input = $request->all();
        if($input['model'] == 'user') {
            $model = $this->userRepository->findWithoutFail(auth()->id());
        }

        if($input['model'] == 'market') {
            $model = $this->marketRepository->findWithoutFail($input['model_id']);
        }

        if($input['model'] == 'product') {
            $model = $this->productRepository->findWithoutFail($input['model_id']);
        }

        if($input['model'] == 'payment') {
            $model = $this->paymentRepository->findWithoutFail($input['model_id']);
        }

        try {
            if ($model->hasMedia($input['field'])) {
                $model->getFirstMedia($input['field'])->delete();
                return $this->sendResponse(true, 'Media deleted successfully');
            }
            
        } catch (ValidatorException $e) {
            return $this->sendResponse(false, $e->getMessage());
        }
    }

    public function removeMedia(UploadRequest $request)
    {
        $input = $request->all();

        if($input['model'] == 'market') {
            $model = $this->marketRepository->findWithoutFail($input['model_id']);
        }

        if($input['model'] == 'product') {
            $model = $this->productRepository->findWithoutFail($input['model_id']);
        }

        if($input['model'] == 'paymnent') {
            $model = $this->paymentRepository->findWithoutFail($input['model_id']);
        }

        try {
            if ($model->hasMedia($input['field'])) {
                $model->deleteMedia($input['id']);
                return $this->sendResponse(true, 'Media deleted successfully');
            }
            
        } catch (ValidatorException $e) {
            return $this->sendResponse(false, $e->getMessage());
        }
    }
}
