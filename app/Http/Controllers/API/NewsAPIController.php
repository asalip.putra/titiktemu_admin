<?php
/**
 * File name: NewsAPIController.php
 * Last modified: 2020.05.04 at 09:04:18
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers\API;


use App\Criteria\News\ActiveCriteria;
use App\Http\Controllers\Controller;
use App\Models\News;
use App\Repositories\NewsRepository;
use Flash;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class NewsController
 * @package App\Http\Controllers\API
 */
class NewsAPIController extends Controller
{
    /** @var  NewsyRepository */
    private $newsRepository;

    public function __construct(NewsRepository $newsRepo)
    {
        $this->newsRepository = $newsRepo;
    }

    /**
     * Display a listing of the News.
     * GET|HEAD /News
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            $this->newsRepository->pushCriteria(new RequestCriteria($request));
            $this->newsRepository->pushCriteria(new LimitOffsetCriteria($request));
            $this->newsRepository->pushCriteria(new ActiveCriteria());
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $news = $this->newsRepository->all();

        return $this->sendResponse($news->toArray(), 'News retrieved successfully');
    }

    /**
     * Display the specified News.
     * GET|HEAD /news/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        /** @var News $news */
        if (!empty($this->newsRepository)) {
            $this->newsRepository->pushCriteria(new RequestCriteria($request));
            $news = $this->newsRepository->findWithoutFail($id);
        }

        if (empty($news)) {
            return $this->sendError('News not found');
        }

        return $this->sendResponse($news->toArray(), 'News retrieved successfully');
    }
}
