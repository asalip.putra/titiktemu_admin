<?php
/**
 * File name: OrderAPIController.php
 * Last modified: 2020.05.31 at 19:34:40
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers\API;

use App\Criteria\Orders\OrdersOfManagerCriteria;
use App\Criteria\Orders\OrdersOfPeriodCriteria;
use App\Criteria\Orders\OrdersOfStatusesCriteria;
use App\Criteria\Orders\OrdersOfUserCriteria;
use App\Criteria\ProductOrders\ProductOrdersOfManagerCriteria;
use App\Criteria\ProductOrders\ProductOrdersOfPeriodCriteria;
use App\Criteria\ProductOrders\ProductOrdersOfStatusesCriteria;
use App\Criteria\Users\AdminsCriteria;
use App\Events\OrderChangedEvent;
use App\Http\Controllers\Controller;
use App\Models\DeliveryAddress;
use App\Models\MarketsPayout;
use App\Models\Order;
use App\Models\Payment;
use App\Notifications\NewOrder;
use App\Notifications\NewOrderAdmin;
use App\Notifications\NewOrderManager;
use App\Notifications\StatusCancelOrder;
use App\Notifications\StatusCancelOrderAdmin;
use App\Notifications\StatusChangedOrder;
use App\Notifications\StatusChangedOrderAdmin;
use App\Repositories\CartRepository;
use App\Repositories\NotificationRepository;
use App\Repositories\OrderRepository;
use App\Repositories\PaymentRepository;
use App\Repositories\ProductOrderRepository;
use App\Repositories\ProductRepository;
use App\Repositories\UserRepository;
use Braintree\Gateway;
use Carbon\Carbon;
use DateTime;
use Flash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Validator\Exceptions\ValidatorException;
use Stripe\Token;

/**
 * Class OrderController
 * @package App\Http\Controllers\API
 */
class OrderAPIController extends Controller
{
    /** @var  OrderRepository */
    private $orderRepository;
    /** @var  ProductOrderRepository */
    private $productOrderRepository;
    /** @var  CartRepository */
    private $cartRepository;
    /** @var  UserRepository */
    private $userRepository;
    /** @var  PaymentRepository */
    private $paymentRepository;
    /** @var  NotificationRepository */
    private $notificationRepository;

    /** @var  ProductRepository */
    private $productRepository;

    /**
     * OrderAPIController constructor.
     * @param OrderRepository $orderRepo
     * @param ProductOrderRepository $productOrderRepository
     * @param CartRepository $cartRepo
     * @param PaymentRepository $paymentRepo
     * @param NotificationRepository $notificationRepo
     * @param UserRepository $userRepository
     */
    public function __construct(OrderRepository $orderRepo, ProductOrderRepository $productOrderRepository, CartRepository $cartRepo, PaymentRepository $paymentRepo, NotificationRepository $notificationRepo, UserRepository $userRepository, ProductRepository $productRepository)
    {
        $this->orderRepository = $orderRepo;
        $this->productOrderRepository = $productOrderRepository;
        $this->cartRepository = $cartRepo;
        $this->userRepository = $userRepository;
        $this->paymentRepository = $paymentRepo;
        $this->notificationRepository = $notificationRepo;
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the Order.
     * GET|HEAD /orders
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        
        try {
            $this->orderRepository->pushCriteria(new RequestCriteria($request));
            $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
            $this->orderRepository->pushCriteria(new OrdersOfStatusesCriteria($request));
            $this->orderRepository->pushCriteria(new OrdersOfUserCriteria(auth()->id()));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $orders = $this->orderRepository->all();

        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }

    /**
     * Display the specified Order.
     * GET|HEAD /orders/{id}
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        /** @var Order $order */
        if (!empty($this->orderRepository)) {
            try {
                $this->orderRepository->pushCriteria(new RequestCriteria($request));
                $this->orderRepository->pushCriteria(new LimitOffsetCriteria($request));
            } catch (RepositoryException $e) {
                return $this->sendError($e->getMessage());
            }
            $order = $this->orderRepository->findWithoutFail($id);
        }

        if (empty($order)) {
            return $this->sendError('Order not found');
        }

        return $this->sendResponse($order->toArray(), 'Order retrieved successfully');


    }

    /**
     * Store a newly created Order in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $payment = $request->only('payment');
        if (isset($payment['payment']) && $payment['payment']['method']) {
            if ($payment['payment']['method'] == "Credit Card (Stripe Gateway)") {
                return $this->stripPayment($request);
            } else {
                return $this->cashPayment($request);

            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    private function stripPayment(Request $request)
    {
        $input = $request->all();
        $amount = 0;
        try {
            $user = $this->userRepository->findWithoutFail($input['user_id']);
            if (empty($user)) {
                return $this->sendError('User not found');
            }
            $stripeToken = Token::create(array(
                "card" => array(
                    "number" => $input['stripe_number'],
                    "exp_month" => $input['stripe_exp_month'],
                    "exp_year" => $input['stripe_exp_year'],
                    "cvc" => $input['stripe_cvc'],
                    "name" => $user->name,
                )
            ));
            if ($stripeToken->created > 0) {
                if (empty($input['delivery_address_id'])) {
                    $order = $this->orderRepository->create(
                        $request->only('user_id', 'order_status_id', 'tax', 'hint')
                    );
                } else {
                    $order = $this->orderRepository->create(
                        $request->only('user_id', 'order_status_id', 'tax', 'delivery_address_id', 'delivery_fee', 'hint')
                    );
                }
                foreach ($input['products'] as $productOrder) {
                    $productOrder['order_id'] = $order->id;
                    $amount += $productOrder['price'] * $productOrder['quantity'];
                    $this->productOrderRepository->create($productOrder);
                }
                $amount += $order->delivery_fee;
                $amountWithTax = $amount + ($amount * $order->tax / 100);
                $charge = $user->charge((int)($amountWithTax * 100), ['source' => $stripeToken]);
                $payment = $this->paymentRepository->create([
                    "user_id" => $input['user_id'],
                    "description" => trans("lang.payment_order_done"),
                    "price" => $amountWithTax,
                    "status" => $charge->status, // $charge->status
                    "method" => $input['payment']['method'],
                ]);
                $this->orderRepository->update(['payment_id' => $payment->id], $order->id);

                $this->cartRepository->deleteWhere(['user_id' => $order->user_id]);

                // Notification::send($order->productOrders[0]->product->market->users, new NewOrder($order));
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($order->toArray(), __('lang.saved_successfully', ['operator' => __('lang.order')]));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    private function cashPayment(Request $request)
    {
        $input = $request->all();
        $amount = 0;
        try {
            $order = $this->orderRepository->create([
                    'user_id' => $input['user_id'],
                    'order_status_id' => $input['order_status_id'],
                    'tax' => $input['tax'],
                    'delivery_address_id' => $input['delivery_address_id'],
                    'delivery_fee' => $input['delivery_fee'],
                    'hint' => $input['hint'], 
                    'waybill' => $input['waybill'],
                    'lm' => $input['lm'],
                    'type' => $input['type'], 
                    'bank_name' => $input['bank_name'], 
                    'uniq_code' => $input['uniq_code'], 
                    'courier' => $input['type'] == 'Pickup' ? null : $input['courier'], 
                    'meeting_time' => isset($input['meeting_time']) ? new DateTime($input['meeting_time']) : null
                ]
                // $request->only('user_id', 'order_status_id', 'tax', 'delivery_address_id', 'delivery_fee', 'hint', 'type', 'courier', 'meeting_time')
            );

            Log::info($input['products']);
            foreach ($input['products'] as $productOrder) {
                $productOrder['order_id'] = $order->id;
                $amount += $productOrder['price'] * $productOrder['quantity'];
                $this->productOrderRepository->create($productOrder);
            }
            $amount += $order->delivery_fee;
            $amountWithTax = $amount + ($amount * $order->tax / 100);
            $payment = $this->paymentRepository->create([
                "user_id" => $input['user_id'],
                "description" => trans("lang.payment_order_waiting"),
                "price" => $amountWithTax,
                "status" => 'Waiting for Client',
                "method" => $input['payment']['method'],
            ]);

            $order_date = Carbon::now();
            $order_no = 'temuinAja/' . $order->id . '/' . $order_date->format('Y') . '/' . $order_date->format('m') . '/' . $order_date->format('d') ;
            $newOrder = $this->orderRepository->update(['payment_id' => $payment->id, 'order_no' => $order_no], $order->id);

            $newOrder->delivery_address = null;
            $newOrder->payment = null;
            $newOrder->market_payouts = null;

            $this->cartRepository->deleteWhere(['user_id' => $order->user_id]);

            if (setting('enable_notifications', false)) {
                $this->userRepository->pushCriteria(new AdminsCriteria(auth()->id()));
                $admin = $this->userRepository->all();
                Notification::send([$admin[0]], new NewOrderAdmin($newOrder));
                Notification::send([$order->productOrders[0]->product->market->users[0]], new NewOrderManager($newOrder));
            }

        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($newOrder, __('lang.saved_successfully', ['operator' => __('lang.order')]));
    }

    /**
     * Update the specified Order in storage.
     *
     * @param int $id
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {
        $oldOrder = $this->orderRepository->findWithoutFail($id);
        if (empty($oldOrder)) {
            return $this->sendError('Order not found');
        }
        $oldStatus = $oldOrder->payment->status;
        $input = $request->all();

        try {
            if(isset($input['certificate_date'])) {
                $input['certificate_date'] = new DateTime($input['certificate_date']);
            }
            $order = $this->orderRepository->update($input, $id);

            if (isset($input['active']) && $input['active'] != $oldOrder->active) {
                $sale_qty = 0;
                foreach ($order->productOrders as $productOrder) {
                    $product = $this->productRepository->findWithoutFail($productOrder->product_id);
                    $sale_qty += $productOrder->quantity;
                    $qty = $product->package_items_count + $sale_qty;
                    $this->productRepository->update(['package_items_count' => $qty], $productOrder->product_id);
                    $sale_qty = 0;
                }
            }

            if (isset($input['order_status_id']) && $input['order_status_id'] == 5 && !empty($order)) {
                $this->paymentRepository->update(['status' => 'Paid'], $order['payment_id']);
            }
            event(new OrderChangedEvent($oldStatus, $order));

            if (setting('enable_notifications', false)) {
                $this->userRepository->pushCriteria(new AdminsCriteria(auth()->id()));
                $admin = $this->userRepository->all();
                if (isset($input['order_status_id']) && $input['order_status_id'] != $oldOrder->order_status_id) {
                    Notification::send([$order->user], new StatusChangedOrder($order));
                    Notification::send([$admin[0]], new StatusChangedOrderAdmin($order));
                }
                if (isset($input['active']) && $input['active'] != $oldOrder->active) {
                    Notification::send([$order->user], new StatusCancelOrder($order));
                    Notification::send([$admin[0]], new StatusCancelOrderAdmin($order));
                }

            }

        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($order->toArray(), __('lang.saved_successfully', ['operator' => __('lang.order')]));
    }

    /**
     * Display a listing of the Cart.
     * GET|HEAD /carts
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function histories(Request $request)
    {
        try {
            $this->orderRepository->pushCriteria(new RequestCriteria($request));
            $this->orderRepository->pushCriteria(new OrdersOfPeriodCriteria($request));
            $this->orderRepository->pushCriteria(new OrdersOfStatusesCriteria($request));
            $this->orderRepository->pushCriteria(new OrdersOfManagerCriteria(auth()->id()));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $orders = $this->orderRepository->all();

        return $this->sendResponse($orders->toArray(), 'Orders retrieved successfully');
    }


    /**
     * Display a listing of the Cart.
     * GET|HEAD /carts
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function tracking(Request $request, $id)
    {
        $input = $request->all();

        $data = (object) array("kurir" => strtolower($input['expedisi']), "resi" => $input['no_resi']);

        $postdata = json_encode($data);

        $ch = curl_init("https://pluginongkoskirim.com/cek-tarif-ongkir/front/resi-amp");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $result = curl_exec($ch);
        curl_close($ch);

        //Rajaongkir 
        // $curl = curl_init();

        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => "https://pro.rajaongkir.com/api/waybill",
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => "",
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 30,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => "POST",
        //     CURLOPT_POSTFIELDS => "waybill=" . $input['no_resi'] . "&courier=" . strtolower($input['expedisi']),
        //     CURLOPT_HTTPHEADER => array(
        //         "content-type: application/x-www-form-urlencoded",
        //         "key: 7d128f1381c4001f36abaa9846b1c829"
        //     ),
        // ));

        // $result = curl_exec($curl);
        // $err = curl_error($curl);

        $jsondata = json_decode($result, true);
        if($jsondata['data']['found']) {
            $delivered = $jsondata['data']['detail']['status'] == "DELIVERED" ? true : false;
            $manifest = array();
            foreach($jsondata['data']['detail']['history'] as $chk) {
                $manifest[] = array(
                    "manifest_code" => "",
                    "manifest_description" => $chk['desc'],
                    "manifest_date" => $chk['time'],
                    "manifest_time" => "",
                    "city_name" => $chk['position']
                );
            }

            $delivery = array(
                "no_resi" => $input['no_resi'],
                "expedisi" => $input['expedisi'],
                "delivered" => $delivered,
                "summary" => array(
                    "courier_code" => $jsondata['data']['detail']['kurir'][0],
                    "courier_name" => $jsondata['data']['detail']['kurir'][0],
                    "waybill_number" => $jsondata['data']['detail']['code'],
                    "service_code" => $jsondata['data']['detail']['service'],
                    "waybill_date" => "",
                    "shipper_name" => $jsondata['data']['detail']["shipper"]["name"],
                    "receiver_name" => $jsondata['data']['detail']["consignee"]["name"],
                    "origin" => $jsondata['data']['detail']['origin'],
                    "destination" => $jsondata['data']['detail']['destination'],
                    "status" => $jsondata['data']['detail']['status']
                ),
                "manifest" => $manifest
            );
            return $this->sendResponse($delivery, 'Resi retrieved successfully');
        }
    }

    /**
     * Display a listing of the Cart.
     * GET|HEAD /carts
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function sale(Request $request)
    {
        $input = $request->all();
        $status_omzet = array(6, 7);
        
        try{
            $this->productOrderRepository->pushCriteria(new ProductOrdersOfManagerCriteria(auth()->id()));
            $this->productOrderRepository->pushCriteria(new ProductOrdersOfStatusesCriteria($status_omzet));
            $this->productOrderRepository->pushCriteria(new ProductOrdersOfPeriodCriteria($input));
            $products = $this->productOrderRepository->all('orders.id');
            $product_sale = $this->productOrderRepository->all('orders.id')->sum('quantity');
            $product_omzet = $this->productOrderRepository->all('orders.id')->sum('price');
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }

        $product_recap = array();
        foreach ($products as $product) {
            if (isset($product_recap[$product['product_id']])) {
                $product_recap[$product['product_id']]['quantity'] += $product->quantity;
            } else {
                $product_recap[$product['product_id']] = $product;
            }
        }
        
        $result = array(
            "product_sale" => $product_sale,
            "product_omzet" => $product_omzet,
            "product_recap" => array_values($product_recap)
        );
        return $this->sendResponse($result, 'Count retrieved successfully');
    }

    /**
     * Display a listing of the Cart.
     * GET|HEAD /carts
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paidStatus(Request $request)
    {
        $input = $request->all();
        $status = array($input['status']);
        try{
            $this->productOrderRepository->pushCriteria(new ProductOrdersOfManagerCriteria(auth()->id()));
            $this->productOrderRepository->pushCriteria(new ProductOrdersOfStatusesCriteria($status));
            $this->productOrderRepository->pushCriteria(new ProductOrdersOfPeriodCriteria($input));
            $product_status = $this->productOrderRepository->all('orders.id')->sum('price');

        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }

        $result = array(
            "total" => $product_status
        );
        return $this->sendResponse($result, 'Count retrieved successfully');
    }
}
