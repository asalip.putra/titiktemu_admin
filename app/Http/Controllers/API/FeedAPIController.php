<?php
/**
 * File name: NewsAPIController.php
 * Last modified: 2020.05.04 at 09:04:18
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Models\Feed;
use App\Criteria\Users\ClientsCriteria;
use App\Repositories\FeedRepository;
use App\Repositories\FeedLikeRepository;
use App\Repositories\UploadRepository;
use App\Repositories\UserRepository;
use App\Notifications\NewFeed;
use App\Notifications\NewLike;
use App\Repositories\NotificationRepository;
use Illuminate\Support\Facades\Notification;
use Flash;
use Illuminate\Http\Request;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Exceptions\RepositoryException;

/**
 * Class FeedController
 * @package App\Http\Controllers\API
 */
class FeedAPIController extends Controller
{
    /** @var  FeedRepository */
    private $feedRepository;

    /** @var  FeedLikeRepository */
    private $feedLikeRepository;

    /** @var  UploadRepository */
    private $uploadRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /** @var  NotificationRepository */
    private $notificationRepository;

    public function __construct(FeedRepository $feedRepo, UploadRepository $uploadRepo, FeedLikeRepository $feedLikeRepo, UserRepository $userRepo, NotificationRepository $notificationRepo)
    {
        $this->feedRepository = $feedRepo;
        $this->uploadRepository = $uploadRepo;
        $this->feedLikeRepository = $feedLikeRepo;
        $this->userRepository = $userRepo;
        $this->notificationRepository = $notificationRepo;
    }

    /**
     * Display a listing of the News.
     * GET|HEAD /News
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $input = $request->all();
        try{
            $this->feedRepository->pushCriteria(new RequestCriteria($request));
            $this->feedRepository->pushCriteria(new LimitOffsetCriteria($request));
        } catch (RepositoryException $e) {
            return $this->sendError($e->getMessage());
        }
        $feeds = $this->feedRepository->paginate($input['paginate']);

        $result = array();
        foreach ($feeds as $key => $feed) {
            $result[] = $feed;
        }

        return $this->sendResponse($result, 'Feed retrieved successfully');
    }

    /**
     * Display the specified News.
     * GET|HEAD /news/{id}
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request, $id)
    {
        /** @var News $news */
        if (!empty($this->feedRepository)) {
            $this->feedRepository->pushCriteria(new RequestCriteria($request));
            $feed = $this->feedRepository->findWithoutFail($id);
        }

        if (empty($feed)) {
            return $this->sendError('Feed not found');
        }

        return $this->sendResponse($feed->toArray(), 'Feed retrieved successfully');
    }

     /**
     * Store a newly created Product in storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $input = $request->all();
        try {
            $feed = $this->feedRepository->create($input);
            if (isset($input['image']) && $input['image']) {
                $cacheUpload = $this->uploadRepository->getByUuid($input['image']);
                $mediaItem = $cacheUpload->getMedia('image')->first();
                $mediaItem->copy($feed, 'image');
            }

            if (setting('enable_notifications', false)) {
                if (isset($input['description'])) {
                    $this->userRepository->pushCriteria(new ClientsCriteria(auth()->id()));
                    $users = $this->userRepository->all();
                    
                    foreach ($users as $user) {
                        Notification::send([$user], new NewFeed($feed));
                    }
                }
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($feed->toArray(), __('lang.saved_successfully', ['operator' => __('lang.product')]));
    }

    public function like(Request $request, $id)
    {
        $input = $request->all();
        $input['feed_id'] = $id;
        try {
            $feedLike = $this->feedLikeRepository->create($input);

            if (setting('enable_notifications', false)) {
                if (isset($input['feed_id'])) {
                    $feed = $this->feedRepository->findWithoutFail($input['feed_id']);
                    $user = $this->userRepository->findWithoutFail($input['user_id']);
                    $receiver = $this->userRepository->findWithoutFail($feed->user_id);
                    Notification::send([$receiver], new NewLike($feed, $user));
                }
            }
        } catch (ValidatorException $e) {
            return $this->sendError($e->getMessage());
        }

        return $this->sendResponse($feedLike->toArray(), __('lang.saved_successfully', ['operator' => __('lang.product')]));
    }

    /**
     * Remove the specified Favorite from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $feed = $this->feedRepository->findWithoutFail($id);

        if (empty($feed)) {
            return $this->sendError('Feed not found');

        }

        $this->feedRepository->delete($id);

        return $this->sendResponse($feed, __('lang.deleted_successfully',['operator' => __('lang.favorite')]));

    }
}
