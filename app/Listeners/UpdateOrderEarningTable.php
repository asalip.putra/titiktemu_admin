<?php
/**
 * File name: UpdateOrderEarningTable.php
 * Last modified: 2020.05.05 at 17:03:49
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

namespace App\Listeners;

use App\Criteria\Earnings\EarningOfMarketCriteria;
use App\Repositories\EarningRepository;
use App\Repositories\OrderRepository;

class UpdateOrderEarningTable
{
    /**
     * @var EarningRepository
     */
    private $earningRepository;

    /** @var  OrderRepository */
    private $orderRepository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EarningRepository $earningRepository, OrderRepository $orderRepo)
    {
        //
        $this->earningRepository = $earningRepository;
        $this->orderRepository = $orderRepo;
    }

    /**
     * Handle the event.
     *oldOrder
     * updatedOrder
     * @param object $event
     * @return void
     */
    public function handle($event)
    {
        if ($event->oldStatus != $event->updatedOrder->payment->status) {
            $order = $this->orderRepository->findWithoutFail($event->updatedOrder->productOrders[0]->order_id);
            $this->earningRepository->pushCriteria(new EarningOfMarketCriteria($event->updatedOrder->productOrders[0]->product->market->id));
            $market = $this->earningRepository->first();
//            dd($market);
            $amount = 0;
            $commission = 0;

            // test if order delivered to client
            if (!empty($market)) {
                foreach ($event->updatedOrder->productOrders as $productOrder) {
                    $amount += ($productOrder['price'] * $productOrder['quantity']) + $order['delivery_fee'] + $order['uniq_code'];
                    $commission += $productOrder['product']['capacity'] * floatval(setting('commission_value'));
                }
                
                if ($event->updatedOrder->payment->status == 'Paid') {
                    $market->total_orders++;
                    $market->total_earning += $amount;
                    $market->admin_earning += $commission;
                    $market->market_earning += $amount - $commission;
                    $market->delivery_fee += $event->updatedOrder->delivery_fee;
                    $market->tax += $amount * $event->updatedOrder->tax / 100;
                    $market->save();
                } elseif ($event->oldStatus == 'Paid') {
                    $market->total_orders--;
                    $market->total_earning -= $amount;
                    $market->admin_earning -= $commission;
                    $market->market_earning -= $amount - $commission;
                    $market->delivery_fee -= $event->updatedOrder->delivery_fee;
                    $market->tax -= $amount * $event->updatedOrder->tax / 100;
                    $market->save();
                }
            }

        }
    }
}
