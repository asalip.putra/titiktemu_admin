<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\News::class, function (Faker $faker) {
    return [
        'title'=>$faker->randomElement(['Berita Hari ini','Demo Buruh']),
        'description'=>$faker->sentences(5,true),
    ];
});
