<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Feed::class, function (Faker $faker) {
    return [
        'description'=>$faker->sentences(5,true),
    ];
});
