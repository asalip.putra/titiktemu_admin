<div class='btn-group btn-group-sm'>
    @if(in_array($id,$myComments))
        @can('comments.show')
            <a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.view_details')}}" href="{{ route('comments.show', $id) }}" class='btn btn-link'>
                <i class="fa fa-eye"></i> </a>
        @endcan

        @can('comments.edit')
            <a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.comment_edit')}}" href="{{ route('comments.edit', $id) }}" class='btn btn-link'>
                <i class="fa fa-edit"></i> </a>
        @endcan

        @can('comments.destroy')
            {!! Form::open(['route' => ['comments.destroy', $id], 'method' => 'delete']) !!}
            {!! Form::button('<i class="fa fa-trash"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-link text-danger',
            'onclick' => "return confirm('Are you sure?')"
            ]) !!}
            {!! Form::close() !!}
        @endcan
    @endif
</div>
