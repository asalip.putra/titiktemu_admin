@if($customFields)
<h5 class="col-12 pb-4">{!! trans('lang.main_fields') !!}</h5>
@endif
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">
<!-- Title Field -->
<div class="form-group row ">
  {!! Form::label('title', trans("lang.news_title"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::text('title', null,  ['class' => 'form-control','placeholder'=>  trans("lang.news_title_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.news_title_help") }}
    </div>
  </div>
</div>

<!-- Description Field -->
<div class="form-group row ">
  {!! Form::label('description', trans("lang.news_description"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::textarea('description', null, ['class' => 'form-control','placeholder'=>
     trans("lang.news_description_placeholder")  ]) !!}
    <div class="form-text text-muted">{{ trans("lang.news_description_help") }}</div>
  </div>
</div>
</div>
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">

<!-- Title Field -->
<div class="form-group row ">
  {!! Form::label('url', trans("lang.news_url"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::text('url', null,  ['class' => 'form-control','placeholder'=>  trans("lang.news_url_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.news_url_help") }}
    </div>
  </div>
</div>
<!-- Image Field -->
<div class="form-group row">
  {!! Form::label('image', trans("lang.news_image"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <div style="width: 100%" class="dropzone image" id="image" data-field="image">
      <input type="hidden" name="image">
    </div>
    <a href="#loadMediaModal" data-dropzone="image" data-toggle="modal" data-target="#mediaModal" class="btn btn-outline-{{setting('theme_color','primary')}} btn-sm float-right mt-1">{{ trans('lang.media_select')}}</a>
    <div class="form-text text-muted w-50">
      {{ trans("lang.news_image_help") }}
    </div>
  </div>
</div>
@prepend('scripts')
<script type="text/javascript">
    var varnewsimageable = '';
    @if(isset($news) && $news->hasMedia('image'))
    varnewsimageable = {
        name: "{!! $news->getFirstMedia('image')->name !!}",
        size: "{!! $news->getFirstMedia('image')->size !!}",
        type: "{!! $news->getFirstMedia('image')->mime_type !!}",
        collection_name: "{!! $news->getFirstMedia('image')->collection_name !!}"};
    @endif
    var dz_varnewsimageable = $(".dropzone.image").dropzone({
        url: "{!!url('uploads/store')!!}",
        addRemoveLinks: true,
        maxFiles: 1,
        init: function () {
        @if(isset($news) && $news->hasMedia('image'))
            dzInit(this,varnewsimageable,'{!! url($news->getFirstMediaUrl('image','thumb')) !!}')
        @endif
        },
        accept: function(file, done) {
            dzAccept(file,done,this.element,"{!!config('medialibrary.icons_folder')!!}");
        },
        sending: function (file, xhr, formData) {
            dzSending(this,file,formData,'{!! csrf_token() !!}');
        },
        maxfilesexceeded: function (file) {
          dz_varnewsimageable[0].mockFile = '';
            dzMaxfile(this,file);
        },
        complete: function (file) {
            dzComplete(this, file, varnewsimageable, dz_varnewsimageable[0].mockFile);
            dz_varnewsimageable[0].mockFile = file;
        },
        removedfile: function (file) {
            dzRemoveFile(
                file, varnewsimageable, '{!! url("news/remove-media") !!}',
                'image', '{!! isset($news) ? $news->id : 0 !!}', '{!! url("uploads/clear") !!}', '{!! csrf_token() !!}'
            );
        }
    });
    dz_varnewsimageable[0].mockFile = varnewsimageable;
    dropzoneFields['image'] = dz_varnewsimageable;
</script>
@endprepend
</div>
@if($customFields)
<div class="clearfix"></div>
<div class="col-12 custom-field-container">
  <h5 class="col-12 pb-4">{!! trans('lang.custom_field_plural') !!}</h5>
  {!! $customFields !!}
</div>
@endif
<div class="col-12 custom-field-container">
  <h5 class="col-12 pb-4">{!! trans('lang.admin_area') !!}</h5>
  <div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">
      <!-- Users Field -->
      <div class="form-group row ">
        {!! Form::label('user_id', trans("lang.news_users"),['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
          {!! Form::select('user_id', $user, null, ['class' => 'select2 form-control']) !!}
          <div class="form-text text-muted">{{ trans("lang.news_users_help") }}</div>
        </div>
      </div>

      <div class="form-group row ">
          {!! Form::label('active', trans("lang.news_active"),['class' => 'col-3 control-label text-right']) !!}
          <div class="checkbox icheck">
              <label class="col-9 ml-2 form-check-inline">
                  {!! Form::hidden('active', 0) !!}
                  {!! Form::checkbox('active', 1, null) !!}
              </label>
          </div>
      </div>
      
  </div>
</div>
<!-- Submit Field -->
<div class="form-group col-12 text-right">
  <button type="submit" class="btn btn-{{setting('theme_color')}}" ><i class="fa fa-save"></i> {{trans('lang.save')}} {{trans('lang.news')}}</button>
  <a href="{!! route('news.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.cancel')}}</a>
</div>
