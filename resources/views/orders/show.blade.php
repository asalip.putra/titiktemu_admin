@extends('layouts.app')

@section('content')
<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark">{{trans('lang.order_plural')}}<small class="ml-3 mr-3">|</small><small>{{trans('lang.order_desc')}}</small></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('/dashboard')}}"><i class="fa fa-dashboard"></i> {{trans('lang.dashboard')}}</a></li>
          <li class="breadcrumb-item"><a href="{!! route('orders.index') !!}">{{trans('lang.order_plural')}}</a>
          </li>
          <li class="breadcrumb-item active">{{trans('lang.order')}}</li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<div class="content">
  <div class="card">
    <div class="card-header d-print-none">
      <ul class="nav nav-tabs align-items-end card-header-tabs w-100">
        <li class="nav-item">
          <a class="nav-link" href="{!! route('orders.index') !!}"><i class="fa fa-list mr-2"></i>{{trans('lang.order_table')}}</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="{!! url()->current() !!}"><i class="fa fa-plus mr-2"></i>{{trans('lang.order')}}</a>
        </li>
        <div class="ml-auto d-inline-flex">
          <li class="nav-item">
            <a class="nav-link pt-1" id="printOrder" href="#"><i class="fa fa-print"></i> {{trans('lang.print')}}</a>
          </li>
        </div>
      </ul>
    </div>
    <div class="card-body">
      <div class="row">
        @include('orders.show_fields')
      </div>
      @include('product_orders.table')
      <div class="row">
      <div class="col-5 offset-7">
        <div class="table-responsive table-light">
          <table class="table">
            <tbody><tr>
              <th class="text-right">{{trans('lang.order_subtotal')}}</th>
              <td>{!! getPrice($subtotal) !!}</td>
            </tr>
            <tr>
              <th class="text-right">{{trans('lang.order_delivery_fee')}}</th>
              <td>{!! getPrice($order['delivery_fee'])!!}</td>
            </tr>
            <tr>
              <th class="text-right">{{trans('lang.order_uniq_code')}}</th>
              <td>{!! getPrice($order['uniq_code'])!!}</td>
            </tr>

            <tr>
              <th class="text-right">{{trans('lang.order_total')}}</th>
              <td>{!! getPrice($total)!!}</td>
            </tr>

            <tr>
              <td class="text-right">Sales Commission</td>
              <td>{!! getPrice($commission)!!}</td>
            </tr>
            @if(isset($payment) && $order['order_status_id'] >= '6')
            <tr>
              <td class="text-right">Nilai untuk di transfer ke rekening <b>{!! strtoupper($payment)!!}</b> penjual </td>
              <td>{!! getPrice($transfer)!!}</td>
            </tr>
            @endif
            </tbody></table>
        </div>
      </div>
      </div>
      <div class="clearfix"></div>
      <div class="row d-print-none">
        <!-- Back Field -->
        <div class="form-group col-12 text-right">
        @if(isset($payment) && isset($marketPayout))
            <a href="{!! route('marketsPayouts.show', $marketPayout->id) !!}" class="btn btn-primary"><i class="fa fa-money"></i> {{trans('lang.markets_payout_plural')}}</a>
          @elseif(isset($payment) && $order['order_status_id'] >= 6)
            <a href="{!! route('marketsPayouts.create', ['order_id' => "{$order->id}", 'market_id' => "{$order->productOrders[0]->product->market_id}", 'amount_transfer' => "{$transfer}", 'bank_name' => "{$order->payment->custom_fields['bank']['value']}", 'bank_account' => "{$order->productOrders[0]->product->market->custom_fields['mandiri_account_number']['value']}"]) !!}" class="btn btn-primary"><i class="fa fa-money"></i> {{trans('lang.markets_payout_plural')}}</a>
          @endif

  
          <a href="{!! route('orders.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.back')}}</a>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>
@endsection

@push('scripts')
  <script type="text/javascript">
    $("#printOrder").on("click",function () {
      window.print();
    });
  </script>
@endpush

<a href="https://api.whatsapp.com/send?phone={!! getFormatMobileNumber($order->productOrders[0]->product->market->mobile) !!}&text=" class="wa-float" target="_blank">
<i class="fa fa-whatsapp my-wa-float"></i>
</a>