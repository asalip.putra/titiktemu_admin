<div id="viewPaymentModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header align-items-stretch">
                <h5 class="modal-title flex-grow-1">{!! trans('lang.payment_image') !!}</h5>
            </div>
            <div class="modal-body">
                <div class="row medias-items">
                  @if(isset($payment) && $payment->hasMedia('image'))
                    <img src="{{url($payment->getFirstMediaUrl('image',''))}}" class="gallery-img img-fluid" alt="{{$payment->getFirstMedia('image')->name}}">
                  @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{!! trans('lang.close') !!}</button>
            </div>
        </div>
    </div>
</div>
@if($customFields)
    <h5 class="col-12 pb-4">{!! trans('lang.main_fields') !!}</h5>
@endif
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">
    <!-- User Id Field -->
    <div class="form-group row ">
        {!! Form::label('user_id', trans("lang.order_user_id"),['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::hidden('delivery_address_id', $order->delivery_address_id,  ['class' => 'form-control','placeholder'=>  trans("lang.order_type_placeholder")]) !!}
            {!! Form::select('user_id', $user, null, ['class' => 'select2 form-control']) !!}
            <div class="form-text text-muted">{{ trans("lang.order_user_id_help") }}</div>
        </div>
    </div>

    <!-- Driver Id Field -->
    <div class="form-group row ">
        {!! Form::label('driver_id', trans("lang.order_driver_id"),['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::select('driver_id', $driver, null, ['data-empty'=>trans("lang.order_driver_id_placeholder"),'class' => 'select2 not-required form-control']) !!}
            <div class="form-text text-muted">{{ trans("lang.order_driver_id_help") }}</div>
        </div>
    </div>

    <!-- Order Status Id Field -->
    <div class="form-group row ">
        {!! Form::label('order_status_id', trans("lang.order_order_status_id"),['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::select('order_status_id', $orderStatus, null, ['class' => 'select2 form-control']) !!}
            <div class="form-text text-muted">{{ trans("lang.order_order_status_id_help") }}</div>
        </div>
    </div>

    <!-- Method Field -->
    <div class="form-group row ">
        {!! Form::label('type', trans("lang.order_type"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('type', null,  ['class' => 'form-control','placeholder'=>  trans("lang.order_type_placeholder"), 'readonly' => 'true']) !!}
            <div class="form-text text-muted">
                {{ trans("lang.order_type_help") }}
            </div>
        </div>
    </div>

    <!-- Expires At Field -->
    <div class="form-group row ">
    {!! Form::label('meeting_time', trans("lang.meeting_date"), ['class' => 'col-3 control-label text-right']) !!}
    <div class="col-9">
        {!! Form::text('meeting_time', null,  ['class' => 'form-control datepicker','autocomplete'=>'off','placeholder'=>  trans("lang.meeting_date_placeholder"), 'readonly' => 'true'  ]) !!}
        <div class="form-text text-muted">
        {{ trans("lang.meeting_date_help") }}
        </div>
    </div>
    </div>

    <!-- Status Field -->
    <div class="form-group row ">
        {!! Form::label('status', trans("lang.payment_status"),['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::select('status',
            [
            'Waiting for Client' => 'Menunggu Pembayaran',
            'Confirmed' => 'Menunggu Konfirmasi',
            'Not Paid' => 'Tidak di bayar',
            'Paid' => 'Sudah di bayar',
            ]
            , isset($order->payment) ? $order->payment->status : '', ['class' => 'select2 form-control']) !!}
            <div class="form-text text-muted">{{ trans("lang.payment_status_help") }}</div>
        </div>
    </div>
    <!-- 'Boolean active Field' -->
    <div class="form-group row ">
        {!! Form::label('active', trans("lang.order_active"),['class' => 'col-3 control-label text-right']) !!}
        <div class="checkbox icheck">
            <label class="col-9 ml-2 form-check-inline">
                {!! Form::hidden('active', 0) !!}
                {!! Form::checkbox('active', 1, null) !!}
            </label>
        </div>
    </div>

</div>
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">

    <!-- Tax Field -->
    <div class="form-group row ">
        {!! Form::label('tax', trans("lang.order_tax"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::number('tax', null,  ['class' => 'form-control', 'step'=>"any",'placeholder'=>  trans("lang.order_tax_placeholder")]) !!}
            <div class="form-text text-muted">
                {{ trans("lang.order_tax_help") }}
            </div>
        </div>
    </div>

    <!-- delivery_fee Field -->
    <div class="form-group row ">
        {!! Form::label('delivery_fee', trans("lang.order_delivery_fee"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::number('delivery_fee', null,  ['class' => 'form-control','step'=>"any",'placeholder'=>  trans("lang.order_delivery_fee_placeholder"), 'readonly' => 'true']) !!}
            <div class="form-text text-muted">
                {{ trans("lang.order_delivery_fee_help") }}
            </div>
        </div>
    </div>
    <!-- delivery_fee Field -->
    <div class="form-group row ">
        {!! Form::label('uniq_code', trans("lang.order_uniq_code"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::number('uniq_code', null,  ['class' => 'form-control','step'=>"any",'placeholder'=>  trans("lang.order_uniq_code_placeholder"), 'readonly' => 'true']) !!}
            <div class="form-text text-muted">
                {{ trans("lang.order_uniq_code_help") }}
            </div>
        </div>
    </div>
    <!-- Hint Field -->
    <div class="form-group row ">
        {!! Form::label('hint', trans("lang.order_hint"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::textarea('hint', null, ['class' => 'form-control','placeholder'=>
             trans("lang.order_hint_placeholder")  ]) !!}
            <div class="form-text text-muted">{{ trans("lang.order_hint_help") }}</div>
        </div>
    </div>
    <!-- waybill Field -->
    <div class="form-group row ">
        {!! Form::label('waybill', trans("lang.order_waybill"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('waybill', null, ['class' => 'form-control','placeholder'=>
             trans("lang.order_waybill_placeholder")  ]) !!}
            <div class="form-text text-muted">{{ trans("lang.order_waybill_help") }}</div>
        </div>
    </div>
    <!-- lm Field -->
    <div class="form-group row ">
        {!! Form::label('lm', trans("lang.order_lm"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('lm', null, ['class' => 'form-control','placeholder'=>
             trans("lang.order_lm_placeholder")  ]) !!}
            <div class="form-text text-muted">{{ trans("lang.order_lm_help") }}</div>
        </div>
    </div>
</div>
<hr />
@if($payment)
<div class="col-12 custom-field-container">
    <h5 class="col-12 pb-4">Payment</h5>
    <div class="clearfix"></div>
    <div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">

        <div class="form-group row col-12">
            {!! Form::label('id', 'Id:', ['class' => 'col-3 control-label text-right']) !!}
            <div class="col-9">
                <p>{!! $payment->id !!}</p>
            </div>
        </div>
        <!-- Price Field -->
        <div class="form-group row col-12">
            {!! Form::label('price', 'Price:', ['class' => 'col-3 control-label text-right']) !!}
            <div class="col-9">
                <p>{!! $payment->price !!}</p>
            </div>
        </div>
        <!-- Description Field -->
        <div class="form-group row col-12">
            {!! Form::label('method', 'Method:', ['class' => 'col-3 control-label text-right']) !!}
            <div class="col-9">
                <p>{!! $payment->method !!}</p>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
    <div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">

        <!-- Description Field -->
        <div class="form-group row col-12">
            {!! Form::label('status', 'Status:', ['class' => 'col-3 control-label text-right']) !!}
            <div class="col-9">
                <p>{!! $payment->status !!}</p>
            </div>
        </div>

        <!-- Description Field -->
        <div class="form-group row col-12">
            {!! Form::label('status', 'View Transfer:', ['class' => 'col-3 control-label text-right']) !!}
            <div class="col-9">
            @if(isset($payment) && $payment->hasMedia('image'))
                <img src="{{url($payment->getFirstMediaUrl('image',''))}}" class="gallery-img img-fluid" alt="{{$payment->getFirstMedia('image')->name}}">
                <a href="#loadPaymentModal" data-toggle="modal" data-target="#viewPaymentModal">{{trans('lang.view_details')}}</a>
            @endif
            </div>
        </div>
    </div>
</div>
@endif

@if($paymentCustomFields)
    <div class="clearfix"></div>
    <div class="col-12 custom-field-container">
        <h5 class="col-12 pb-4">Pembayaran Pembeli</h5>
        {!! $paymentCustomFields !!}
    </div>
@endif

@if($customFields)
    <div class="clearfix"></div>
    <div class="col-12 custom-field-container">
        <h5 class="col-12 pb-4">{!! trans('lang.custom_field_plural') !!}</h5>
        {!! $customFields !!}
    </div>
@endif
<!-- Submit Field -->
<div class="form-group col-12 text-right">
    <button type="submit" class="btn btn-{{setting('theme_color')}}"><i class="fa fa-save"></i> {{trans('lang.save')}} {{trans('lang.order')}}</button>
    <a href="{!! route('orders.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.cancel')}}</a>
</div>
