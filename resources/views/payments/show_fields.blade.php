<div id="viewPaymentModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header align-items-stretch">
                <h5 class="modal-title flex-grow-1">{!! trans('lang.market_image') !!}</h5>
            </div>
            <div class="modal-body">
                <div class="row medias-items">
                  @if(isset($payment) && $payment->hasMedia('image'))
                    <img src="{{url($payment->getFirstMediaUrl('image',''))}}" class="gallery-img img-fluid" alt="{{$payment->getFirstMedia('image')->name}}">
                  @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{!! trans('lang.close') !!}</button>
            </div>
        </div>
    </div>
</div>

<!-- Id Field -->
<div class="form-group row col-6">
  {!! Form::label('id', 'Id:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $payment->id !!}</p>
  </div>

  {!! Form::label('price', 'Price:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! getPrice($payment->price) !!}</p>
  </div>

  {!! Form::label('order', 'Order:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $order->order_no !!}</p>
  </div>

  {!! Form::label('user', 'User:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $user->name !!}</p>
  </div>

  {!! Form::label('status', 'Status:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p><span class='badge badge-success'> {!! $payment->status !!}</span></p>
  </div>
  
  {!! Form::label('method', 'Method:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $payment->method !!}</p>
  </div>
  
</div>

<div class="clearfix"></div>

<!-- Created At Field -->
<div class="form-group row col-6">

  {!! Form::label('image', 'Transfer Proof:', ['class' => 'col-4 control-label text-right']) !!}
  <div class="col-8">
    @if(isset($payment) && $payment->hasMedia('image'))
    <p><img src="{{url($payment->getFirstMediaUrl('image',''))}}" class="gallery-img img-fluid" alt="{{$payment->getFirstMedia('image')->name}}"></p>
    <a href="#loadPaymentModal" data-toggle="modal" data-target="#viewPaymentModal">{{trans('lang.view_details')}}</a>
    @endif
  </div>
  
  {!! Form::label('bank_name', 'Bank Name:', ['class' => 'col-4 control-label text-right']) !!}
  <div class="col-8">
    @if(isset($customFields[0]))
    <p>{!! $customFields[0]->value !!}</p>
    @endif
  </div>
  
  {!! Form::label('account_number', 'Account Number:', ['class' => 'col-4 control-label text-right']) !!}
  <div class="col-8">
    @if(isset($customFields[0]))
    <p>{!! $customFields[1]->value !!}</p>
    @endif
  </div>
  
  {!! Form::label('account_number', 'Amount', ['class' => 'col-4 control-label text-right']) !!}
  <div class="col-8">
    @if(isset($customFields[0]))
    <p>{!! getPrice($customFields[2]->value) !!}</p>
    @endif
  </div>
  
  {!! Form::label('created_at', 'Created At:', ['class' => 'col-4 control-label text-right']) !!}
  <div class="col-8">
    @if(isset($customFields[0]))
    <p>{!! $payment->created_at !!}</p>
    @endif
  </div>
  
  {!! Form::label('updated_at', 'Updated At:', ['class' => 'col-4 control-label text-right']) !!}
  <div class="col-8">
    @if(isset($customFields[0]))
    <p>{!! $payment->updated_at !!}</p>
    @endif
  </div>
</div>

