<div id="viewPaymentModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header align-items-stretch">
                <h5 class="modal-title flex-grow-1">{!! trans('lang.payment_image') !!}</h5>
            </div>
            <div class="modal-body">
                <div class="row medias-items">
                  @if(isset($payment) && $payment->hasMedia('image'))
                    <img src="{{url($payment->getFirstMediaUrl('image',''))}}" class="gallery-img img-fluid" alt="{{$payment->getFirstMedia('image')->name}}">
                  @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{!! trans('lang.close') !!}</button>
            </div>
        </div>
    </div>
</div>
@if($customFields)
    <h5 class="col-12 pb-4">{!! trans('lang.main_fields') !!}</h5>
@endif
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">
    <!-- Price Field -->
    <div class="form-group row ">
        {!! Form::label('price', trans("lang.payment_price"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::number('price', null,  ['class' => 'form-control','placeholder'=>  trans("lang.payment_price_placeholder"), 'readonly' => 'true']) !!}
            <div class="form-text text-muted">
                {{ trans("lang.payment_price_help") }}
            </div>
        </div>
    </div>
    <!-- User Id Field -->
    <div class="form-group row ">
        {!! Form::label('user_id', trans("lang.favorite_user_id"),['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::select('user_id', $user, null, ['class' => 'select2 form-control']) !!}
            <div class="form-text text-muted">{{ trans("lang.favorite_user_id_help") }}</div>
        </div>
    </div>

    <!-- Method Field -->
    <div class="form-group row ">
        {!! Form::label('method', trans("lang.payment_method"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('method', null,  ['class' => 'form-control','placeholder'=>  trans("lang.payment_method_placeholder"), 'readonly' => 'true']) !!}
            <div class="form-text text-muted">
                {{ trans("lang.payment_method_help") }}
            </div>
        </div>
    </div>
    
</div>
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">

    <!-- Description Field -->
    <div class="form-group row ">
        {!! Form::label('description', trans("lang.payment_description"), ['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::text('description', null,  ['class' => 'form-control','placeholder'=>  trans("lang.payment_description_placeholder"), 'readonly' => 'true']) !!}
            <div class="form-text text-muted">
                {{ trans("lang.payment_description_help") }}
            </div>
        </div>
    </div>

    <!-- Status Field -->
    <div class="form-group row ">
        {!! Form::label('status', trans("lang.payment_status"),['class' => 'col-3 control-label text-right']) !!}
        <div class="col-9">
            {!! Form::select('status',
            [
            'Waiting for Client' => 'Menunggu Pembayaran',
            'Confirmed' => 'Menunggu Konfirmasi',
            'Not Paid' => 'Tidak di bayar',
            'Paid' => 'Sudah di bayar',
            ]
            , $payment->status, ['class' => 'select2 form-control']) !!}
            <div class="form-text text-muted">{{ trans("lang.payment_status_help") }}</div>
        </div>
    </div>

    <!-- Image Field -->
    <div class="form-group row">
    {!! Form::label('image', trans("lang.payment_image"), ['class' => 'col-3 control-label text-right']) !!}
    <div class="col-9">
        <div style="width: 100%" class="dropzone image" id="image" data-field="image">
        <input type="hidden" name="image">
        </div>
        <a href="#loadMediaModal" data-dropzone="image" data-toggle="modal" data-target="#mediaModal" class="btn btn-outline-{{setting('theme_color','primary')}} btn-sm float-right mt-1">{{ trans('lang.media_select')}}</a>
        <div class="form-text text-muted w-50">
        @if(isset($payment) && $payment->hasMedia('image'))
            <a href="#loadPaymentModal" data-toggle="modal" data-target="#viewPaymentModal">{{trans('lang.view_details')}}</a>
            @else
        {{ trans("lang.payment_image_help") }}
        @endif
        </div>
    </div>
    </div>
    @prepend('scripts')
<script type="text/javascript">
    var varpaymentimageable = '';
    @if(isset($payment) && $payment->hasMedia('image'))
    varpaymentimageable = {
        name: "{!! $payment->getFirstMedia('image')->name !!}",
        size: "{!! $payment->getFirstMedia('image')->size !!}",
        type: "{!! $payment->getFirstMedia('image')->mime_type !!}",
        collection_name: "{!! $payment->getFirstMedia('image')->collection_name !!}"};
    @endif
    var dz_varpaymentimageable = $(".dropzone.image").dropzone({
        url: "{!!url('uploads/store')!!}",
        addRemoveLinks: true,
        maxFiles: 1,
        init: function () {
        @if(isset($payment) && $payment->hasMedia('image'))
            dzInit(this,varpaymentimageable,'{!! url($payment->getFirstMediaUrl('image','thumb')) !!}')
        @endif
        },
        accept: function(file, done) {
            dzAccept(file,done,this.element,"{!!config('medialibrary.icons_folder')!!}");
        },
        sending: function (file, xhr, formData) {
            dzSending(this,file,formData,'{!! csrf_token() !!}');
        },
        maxfilesexceeded: function (file) {
        dz_varpaymentimageable[0].mockFile = '';
            dzMaxfile(this,file);
        },
        complete: function (file) {
            dzComplete(this, file, varpaymentimageable, dz_varpaymentimageable[0].mockFile);
            dz_varpaymentimageable[0].mockFile = file;
        },
        removedfile: function (file) {
            dzRemoveFile(
                file, varpaymentimageable, '{!! url("payments/remove-media") !!}',
                'image', '{!! isset($payment) ? $payment->id : 0 !!}', '{!! url("uploads/clear") !!}', '{!! csrf_token() !!}'
            );
        }
    });
    dz_varpaymentimageable[0].mockFile = varpaymentimageable;
    dropzoneFields['image'] = dz_varpaymentimageable;
</script>
@endprepend
    </div>

</div>

@if($customFields)
    <div class="clearfix"></div>
    <div class="col-12 custom-field-container">
        <h5 class="col-12 pb-4">{!! trans('lang.custom_field_plural') !!}</h5>
        {!! $customFields !!}
    </div>
@endif
<!-- Submit Field -->
<div class="form-group col-12 text-right">
    <button type="submit" class="btn btn-{{setting('theme_color')}}"><i class="fa fa-save"></i> {{trans('lang.save')}} {{trans('lang.payment')}}</button>
    <a href="{!! route('payments.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.cancel')}}</a>
</div>
