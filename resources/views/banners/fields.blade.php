@if($customFields)
<h5 class="col-12 pb-4">{!! trans('lang.main_fields') !!}</h5>
@endif
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">
<!-- Title Field -->
<div class="form-group row ">
  {!! Form::label('title', trans("lang.banner_title"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::text('title', null,  ['class' => 'form-control','placeholder'=>  trans("lang.banner_title_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.banner_title_help") }}
    </div>
  </div>
</div>

<!-- Description Field -->
<div class="form-group row ">
  {!! Form::label('description', trans("lang.banner_description"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::textarea('description', null, ['class' => 'form-control','placeholder'=>
     trans("lang.banner_description_placeholder")  ]) !!}
    <div class="form-text text-muted">{{ trans("lang.banner_description_help") }}</div>
  </div>
</div>
</div>
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">
<!-- Title Field -->
<div class="form-group row ">
  {!! Form::label('url', trans("lang.menu_item_url"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::text('url', null,  ['class' => 'form-control','placeholder'=>  trans("lang.menu_item_url_placeholder")]) !!}
    <div class="form-text text-muted">
      {{ trans("lang.menu_item_url_help") }}
    </div>
  </div>
</div>
<!-- Image Field -->
<div class="form-group row">
  {!! Form::label('image', trans("lang.banner_image"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <div style="width: 100%" class="dropzone image" id="image" data-field="image">
      <input type="hidden" name="image">
    </div>
    <a href="#loadMediaModal" data-dropzone="image" data-toggle="modal" data-target="#mediaModal" class="btn btn-outline-{{setting('theme_color','primary')}} btn-sm float-right mt-1">{{ trans('lang.media_select')}}</a>
    <div class="form-text text-muted w-50">
      {{ trans("lang.banner_image_help") }}
    </div>
  </div>
</div>
@prepend('scripts')
<script type="text/javascript">
    var varbannerimagable = '';
    @if(isset($banner) && $banner->hasMedia('image'))
    varbannerimagable = {
        name: "{!! $banner->getFirstMedia('image')->name !!}",
        size: "{!! $banner->getFirstMedia('image')->size !!}",
        type: "{!! $banner->getFirstMedia('image')->mime_type !!}",
        collection_name: "{!! $banner->getFirstMedia('image')->collection_name !!}"};
    @endif
    var dz_varbannerimagable = $(".dropzone.image").dropzone({
        url: "{!!url('uploads/store')!!}",
        addRemoveLinks: true,
        maxFiles: 1,
        init: function () {
        @if(isset($banner) && $banner->hasMedia('image'))
            dzInit(this,varbannerimagable,'{!! url($banner->getFirstMediaUrl('image','thumb')) !!}')
        @endif
        },
        accept: function(file, done) {
            dzAccept(file,done,this.element,"{!!config('medialibrary.icons_folder')!!}");
        },
        sending: function (file, xhr, formData) {
            dzSending(this,file,formData,'{!! csrf_token() !!}');
        },
        maxfilesexceeded: function (file) {
          dz_varbannerimagable[0].mockFile = '';
            dzMaxfile(this,file);
        },
        complete: function (file) {
            dzComplete(this, file, varbannerimagable, dz_varbannerimagable[0].mockFile);
            dz_varbannerimagable[0].mockFile = file;
        },
        removedfile: function (file) {
            dzRemoveFile(
                file, varbannerimagable, '{!! url("banners/remove-media") !!}',
                'image', '{!! isset($banner) ? $banner->id : 0 !!}', '{!! url("uploads/clear") !!}', '{!! csrf_token() !!}'
            );
        }
    });
    dz_varbannerimagable[0].mockFile = varbannerimagable;
    dropzoneFields['image'] = dz_varbannerimagable;
</script>
@endprepend
</div>
@if($customFields)
<div class="clearfix"></div>
<div class="col-12 custom-field-container">
  <h5 class="col-12 pb-4">{!! trans('lang.custom_field_plural') !!}</h5>
  {!! $customFields !!}
</div>
@endif
<div class="col-12 custom-field-container">
  <h5 class="col-12 pb-4">{!! trans('lang.admin_area') !!}</h5>
  <div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">

      <div class="form-group row ">
          {!! Form::label('active', trans("lang.banner_active"),['class' => 'col-3 control-label text-right']) !!}
          <div class="checkbox icheck">
              <label class="col-9 ml-2 form-check-inline">
                  {!! Form::hidden('active', 0) !!}
                  {!! Form::checkbox('active', 1, null) !!}
              </label>
          </div>
      </div>
      
  </div>
</div>
<!-- Submit Field -->
<div class="form-group col-12 text-right">
  <button type="submit" class="btn btn-{{setting('theme_color')}}" ><i class="fa fa-save"></i> {{trans('lang.save')}} {{trans('lang.banner')}}</button>
  <a href="{!! route('banners.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.cancel')}}</a>
</div>
