<!-- Id Field -->
<div class="form-group row col-6">
  {!! Form::label('id', 'Id:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $gallery->id !!}</p>
  </div>
</div>

<!-- Description Field -->
<div class="form-group row col-6">
  {!! Form::label('description', 'Description:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $gallery->description !!}</p>
  </div>
</div>

<!-- Image Field -->
<div class="form-group row col-6">
  {!! Form::label('image', 'Image:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p><a href="#loadImageModal" data-toggle="modal" data-target="#viewGalleriesModal" class="btn btn-outline-{{setting('theme_color','primary')}} btn-sm float-right mt-1">
    <img src="{{url($gallery->getFirstMediaUrl('image','thumb'))}}" class="gallery-img img-fluid" alt="{{$gallery->getFirstMedia('image')->name}}" />
    </a></p>
  </div>
</div>

<!-- Market Id Field -->
<div class="form-group row col-6">
  {!! Form::label('market_id', 'Market Id:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $gallery->market_id !!}</p>
  </div>
</div>

<!-- Created At Field -->
<div class="form-group row col-6">
  {!! Form::label('created_at', 'Created At:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $gallery->created_at !!}</p>
  </div>
</div>

<!-- Updated At Field -->
<div class="form-group row col-6">
  {!! Form::label('updated_at', 'Updated At:', ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <p>{!! $gallery->updated_at !!}</p>
  </div>
</div>

