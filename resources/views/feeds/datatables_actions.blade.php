<div class='btn-group btn-group-sm'>
    @if(in_array($id,$myFeeds))
        @can('feeds.show')
            <a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.view_details')}}" href="{{ route('feeds.show', $id) }}" class='btn btn-link'>
                <i class="fa fa-eye"></i> </a>
        @endcan

        @can('feeds.edit')
            <a data-toggle="tooltip" data-placement="bottom" title="{{trans('lang.feed_edit')}}" href="{{ route('feeds.edit', $id) }}" class='btn btn-link'>
                <i class="fa fa-edit"></i> </a>
        @endcan

        @can('feeds.destroy')
            {!! Form::open(['route' => ['feeds.destroy', $id], 'method' => 'delete']) !!}
            {!! Form::button('<i class="fa fa-trash"></i>', [
            'type' => 'submit',
            'class' => 'btn btn-link text-danger',
            'onclick' => "return confirm('Are you sure?')"
            ]) !!}
            {!! Form::close() !!}
        @endcan
    @endif
</div>
