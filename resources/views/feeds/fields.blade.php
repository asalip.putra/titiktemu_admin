@if($customFields)
<h5 class="col-12 pb-4">{!! trans('lang.main_fields') !!}</h5>
@endif
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">
<!-- Description Field -->
<div class="form-group row ">
  {!! Form::label('description', trans("lang.feed_description"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::textarea('description', null, ['class' => 'form-control','placeholder'=>
     trans("lang.feed_description_placeholder")  ]) !!}
    <div class="form-text text-muted">{{ trans("lang.feed_description_help") }}</div>
  </div>
</div>

</div>
<div style="flex: 50%;max-width: 50%;padding: 0 4px;" class="column">
<!-- Image Field -->
<div class="form-group row">
  {!! Form::label('image', trans("lang.feed_image"), ['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    <div style="width: 100%" class="dropzone image" id="image" data-field="image">
      <input type="hidden" name="image">
    </div>
    <a href="#loadMediaModal" data-dropzone="image" data-toggle="modal" data-target="#mediaModal" class="btn btn-outline-{{setting('theme_color','primary')}} btn-sm float-right mt-1">{{ trans('lang.media_select')}}</a>
    <div class="form-text text-muted w-50">
      {{ trans("lang.feed_image_help") }}
    </div>
  </div>
</div>
@prepend('scripts')
<script type="text/javascript">
    var varfeedsimageable = '';
    @if(isset($feed) && $feed->hasMedia('image'))
    varfeedsimageable = {
        name: "{!! $feed->getFirstMedia('image')->name !!}",
        size: "{!! $feed->getFirstMedia('image')->size !!}",
        type: "{!! $feed->getFirstMedia('image')->mime_type !!}",
        collection_name: "{!! $feed->getFirstMedia('image')->collection_name !!}"};
    @endif
    var dz_varfeedsimageable = $(".dropzone.image").dropzone({
        url: "{!!url('uploads/store')!!}",
        addRemoveLinks: true,
        maxFiles: 1,
        init: function () {
        @if(isset($feed) && $feed->hasMedia('image'))
            dzInit(this,varfeedsimageable,'{!! url($feed->getFirstMediaUrl('image','thumb')) !!}')
        @endif
        },
        accept: function(file, done) {
            dzAccept(file,done,this.element,"{!!config('medialibrary.icons_folder')!!}");
        },
        sending: function (file, xhr, formData) {
            dzSending(this,file,formData,'{!! csrf_token() !!}');
        },
        maxfilesexceeded: function (file) {
            dz_varfeedsimageable[0].mockFile = '';
            dzMaxfile(this,file);
        },
        complete: function (file) {
            dzComplete(this, file, varfeedsimageable, varfeedsimageable[0].mockFile);
            dz_varfeedsimageable[0].mockFile = file;
        },
        removedfile: function (file) {
            dzRemoveFile(
                file, varfeedsimageable, '{!! url("feeds/remove-media") !!}',
                'image', '{!! isset($feed) ? $feed->id : 0 !!}', '{!! url("uploads/clear") !!}', '{!! csrf_token() !!}'
            );
        }
    });
    dz_varfeedsimageable[0].mockFile = varfeedsimageable;
    dropzoneFields['image'] = dz_varfeedsimageable;
</script>
@endprepend
<!-- User Id Field -->
<div class="form-group row ">
  {!! Form::label('user_id', trans("lang.feed_users"),['class' => 'col-3 control-label text-right']) !!}
  <div class="col-9">
    {!! Form::select('user_id', $user, null, ['class' => 'select2 form-control']) !!}
    <div class="form-text text-muted">{{ trans("lang.feed_users_help") }}</div>
  </div>
</div>

</div>
@if($customFields)
<div class="clearfix"></div>
<div class="col-12 custom-field-container">
  <h5 class="col-12 pb-4">{!! trans('lang.custom_field_plural') !!}</h5>
  {!! $customFields !!}
</div>
@endif
<!-- Submit Field -->
<div class="form-group col-12 text-right">
  <button type="submit" class="btn btn-{{setting('theme_color')}}" ><i class="fa fa-save"></i> {{trans('lang.save')}} {{trans('lang.feed')}}</button>
  <a href="{!! route('feeds.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> {{trans('lang.cancel')}}</a>
</div>
