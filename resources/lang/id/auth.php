<?php

return [
    'agree' => 'Saya setuju dengan persyaratannya',
    'already_member' => 'Saya sudah memiliki keanggotaan',
    'email' => 'Alamat Email',
    'failed' => 'Akun tidak di temukan',
    'forgot_password' => 'Lupa Password',
    'login' => 'Login',
    'login_facebook' => 'Sign in Facebook',
    'login_google' => 'Sign in Google+',
    'login_title' => 'Silahkan Sign in',
    'login_twitter' => 'Sign in Twitter',
    'logout' => 'Keluar',
    'name' => 'Nama Lengkap',
    'password' => 'Password',
    'password_confirmation' => 'Konfirmasi password',
    'register' => 'Sign In',
    'register_new_member' => 'Registrasi',
    'remember_me' => 'Ingat Saya',
    'remember_password' => 'Saya ingat password kembali ke login',
    'reset_password' => 'Reset',
    'reset_password_title' => 'Masukan Password Baru',
    'reset_title' => 'Email untuk reset password',
    'send_password' => 'Kirim password reset link',
    'throttle' => 'Terlalu banyak upaya login. Silahkan coba ladi dalam :seconds detik.',
];