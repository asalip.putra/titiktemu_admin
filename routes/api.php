<?php
/**
 * File name: api.php
 * Last modified: 2020.04.30 at 08:21:08
 * Author: SmarterVision - https://codecanyon.net/user/smartervision
 * Copyright (c) 2020
 *
 */

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('driver')->group(function () {
    Route::post('login', 'API\Driver\UserAPIController@login');
    Route::post('register', 'API\Driver\UserAPIController@register');
    Route::post('send_reset_link_email', 'API\UserAPIController@sendResetLinkEmail');
    Route::get('user', 'API\Driver\UserAPIController@user');
    Route::get('logout', 'API\Driver\UserAPIController@logout');
    Route::get('settings', 'API\Driver\UserAPIController@settings');
});

Route::prefix('manager')->group(function () {
    Route::post('login', 'API\Manager\UserAPIController@login');
    Route::post('register', 'API\Manager\UserAPIController@register');
    Route::post('send_reset_link_email', 'API\UserAPIController@sendResetLinkEmail');
    Route::get('user', 'API\Manager\UserAPIController@user');
    Route::get('logout', 'API\Manager\UserAPIController@logout');
    Route::get('settings', 'API\Manager\UserAPIController@settings');
});


Route::post('login', 'API\UserAPIController@login');
Route::post('register', 'API\UserAPIController@register');
Route::post('send_reset_link_email', 'API\UserAPIController@sendResetLinkEmail');
Route::get('user', 'API\UserAPIController@user');
Route::get('user/{id}', 'API\UserAPIController@show');
Route::get('users', 'API\UserAPIController@users');
Route::get('logout', 'API\UserAPIController@logout');
Route::get('settings', 'API\UserAPIController@settings');

Route::resource('fields', 'API\FieldAPIController');
Route::resource('categories', 'API\CategoryAPIController');
Route::resource('markets', 'API\MarketAPIController');
Route::get('delivery/cities', 'API\DeliveryAPIController@cities');
Route::get('delivery/city/{id}', 'API\DeliveryAPIController@city');

Route::resource('faq_categories', 'API\FaqCategoryAPIController');
Route::get('products/categories', 'API\ProductAPIController@categories');
Route::resource('products', 'API\ProductAPIController');
Route::resource('galleries', 'API\GalleryAPIController');
Route::resource('product_reviews', 'API\ProductReviewAPIController');


Route::resource('faqs', 'API\FaqAPIController');
Route::resource('market_reviews', 'API\MarketReviewAPIController');
Route::resource('currencies', 'API\CurrencyAPIController');

Route::resource('option_groups', 'API\OptionGroupAPIController');

Route::resource('options', 'API\OptionAPIController');

Route::resource('news', 'API\NewsAPIController');

Route::resource('feed', 'API\FeedAPIController');
Route::post('feed/like/{id}', 'API\FeedAPIController@like');

Route::resource('comment', 'API\CommentAPIController');
Route::get('comment/feed/{id}', 'API\CommentAPIController@feed');
Route::get('comment/reply/{id}', 'API\CommentAPIController@reply');

Route::resource('banners', 'API\BannerAPIController');

Route::middleware('auth:api')->group(function () {
    Route::group(['middleware' => ['role:driver']], function () {
        Route::prefix('driver')->group(function () {
            Route::resource('orders', 'API\OrderAPIController');
            Route::resource('notifications', 'API\NotificationAPIController');
            Route::post('users/{id}', 'API\UserAPIController@update');
            Route::resource('faq_categories', 'API\FaqCategoryAPIController');
            Route::resource('faqs', 'API\FaqAPIController');
        });
    });
    Route::group(['middleware' => ['role:manager']], function () {
        Route::prefix('manager')->group(function () {
            Route::post('users/{id}', 'API\UserAPIController@update');
            Route::get('users/drivers_of_market/{id}', 'API\Manager\UserAPIController@driversOfMarket');
            Route::get('dashboard/{id}', 'API\DashboardAPIController@manager');
            Route::resource('markets', 'API\Manager\MarketAPIController');
            Route::post('markets/{id}', 'API\Manager\MarketAPIController@update');
            Route::resource('products', 'API\Manager\ProductAPIController');
            Route::get('products/market/{id}', 'API\ProductAPIController@market');
            Route::post('products/{id}', 'API\Manager\ProductAPIController@update');
            Route::resource('orders', 'API\Manager\OrderAPIController');
        });
    });
    Route::get('delivery/costs', 'API\DeliveryAPIController@costs');

    Route::post('users/{id}', 'API\UserAPIController@update');

    Route::post('markets', 'API\MarketAPIController@store');

    Route::resource('order_statuses', 'API\OrderStatusAPIController');

    Route::get('orders/sale', 'API\OrderAPIController@sale')->name('orders.sale');

    Route::get('orders/paid_status', 'API\OrderAPIController@paidStatus')->name('orders.status');

    Route::get('orders/histories', 'API\OrderAPIController@histories')->name('orders.histories');

    Route::post('orders/tracking/{id}', 'API\OrderAPIController@tracking')->name('orders.tracking');

    Route::get('payments/byMonth', 'API\PaymentAPIController@byMonth')->name('payments.byMonth');
    Route::resource('payments', 'API\PaymentAPIController');

    Route::get('favorites/exist', 'API\FavoriteAPIController@exist');
    Route::resource('favorites', 'API\FavoriteAPIController');

    Route::resource('orders', 'API\OrderAPIController');

    Route::post('messages/push', 'API\MessageAPIController@push');

    Route::resource('messages', 'API\MessageAPIController');

    Route::resource('product_orders', 'API\ProductOrderAPIController');

    Route::get('notifications/count', 'API\NotificationAPIController@count')->name('notifications.count');

    Route::resource('notifications', 'API\NotificationAPIController');
    
    Route::get('carts/count', 'API\CartAPIController@count')->name('carts.count');
    //todo harus di hapus
    Route::get('carts/delivery', 'API\CartAPIController@delivery')->name('carts.delivery');
    Route::resource('carts', 'API\CartAPIController');

    Route::resource('delivery_addresses', 'API\DeliveryAddressAPIController');

    Route::resource('drivers', 'API\DriverAPIController');

    Route::resource('earnings', 'API\EarningAPIController');

    Route::resource('driversPayouts', 'API\DriversPayoutAPIController');

    Route::resource('marketsPayouts', 'API\MarketsPayoutAPIController');

    Route::resource('coupons', 'API\CouponAPIController')->except([
        'show'
    ]);

    Route::post('uploads/store', 'API\UploadAPIController@store')->name('medias.create');
    Route::post('uploads/clear', 'API\UploadAPIController@clear')->name('medias.clear');
    Route::post('uploads/show', 'API\UploadAPIController@show')->name('medias.show');
    Route::post('uploads/update', 'API\UploadAPIController@update')->name('medias.update');
    Route::post('uploads/remove-media', 'API\UploadAPIController@removeMedia')->name('medias.delete');
});